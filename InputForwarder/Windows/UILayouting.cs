﻿// Use table layout instead of StackLayout for Horizontal and Vertical to automtically scale controls within
#define USE_TABLELAYOUT

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Eto.Forms;
using Eto.Drawing;

using InputForwarder.Hardware;






namespace InputForwarder
{
	public class Style
	{
		public Color defaultBackground             = new Color(.2f,  .2f,  .2f),
		             defaultInteractableBackground = new Color(.18f, .18f, .18f),
		             defaultTextColor              = new Color(.8f,  .8f,  .8f);
		public Font defaultFont   = Fonts.Sans(8);
		public bool defaultBorder = false;
	}

	/// <summary>
	/// Interface to create clean layout code,
	/// add a using static and point to this class to have access to all of those functions statically
	/// </summary>
	public static class UILayouting
	{
		const int DEFAULT_PADDING = 0;

		static readonly Color ISVALID_COLOR = new Color(1, 1, 1, 1);
		static readonly Color ISINVALID_COLOR = new Color(1, 0, 0, 1);
		static Style currentStyle = new Style();









		static UILayouting()
		{
			Apply(currentStyle);
		}


		public static void Apply(Style style)
		{
			currentStyle = style;

			Eto.Style.Add<Control>(null, handler =>
			{
				handler.BackgroundColor = style.defaultBackground;
			});
			Eto.Style.Add<TextControl>(null, handler =>
			{
				handler.Font      = style.defaultFont;
				handler.TextColor = style.defaultTextColor;
			});
			Eto.Style.Add<TextBox>(null, handler =>
			{
				handler.ShowBorder      = style.defaultBorder;
				handler.BackgroundColor = style.defaultInteractableBackground;
			});
			Eto.Style.Add<Button>(null, handler =>
			{
				handler.BackgroundColor = style.defaultInteractableBackground;
			});
			Eto.Style.Add<ListBox>(null, handler =>
			{
				handler.TextColor = style.defaultTextColor;
			});
		}











		public static Label Label(string text = null)
		{
			return Label(out Label l, text);
		}

		public static Label Label(out Label labelOut, string text = null)
		{
			labelOut = new Label
			{
				VerticalAlignment = VerticalAlignment.Center
			};
			if (text != null)
				labelOut.Text = text;
			return labelOut;
		}


		public static Control Horizontal(params Control[] widgets)
		{
			return Horizontal(DEFAULT_PADDING, widgets);
		}

		public static Control Horizontal(int padding = DEFAULT_PADDING, params Control[] widgets)
		{
			TableRow row = new TableRow{ ScaleHeight = true };
			foreach (Control control in widgets)
			{
				row.Cells.Add(new TableCell(control){ ScaleWidth = true });
			}
			return new TableLayout
			{
				Padding = new Padding(padding),
				Rows =
				{
					row
				}
			};
		}

		public static Control Vertical(params Control[] widgets)
		{
			return Vertical(DEFAULT_PADDING, widgets);
		}

		public static Control Vertical(int padding = DEFAULT_PADDING, params Control[] widgets)
		{
			TableLayout tl = new TableLayout{Padding = new Padding(padding)};

			foreach (Control control in widgets)
			{
				tl.Rows.Add( new TableRow(new TableCell(control){ScaleWidth = true}){ ScaleHeight = true } );
			}
			return tl;
		}






		public static TextBox TextBox(OnTextboxChanged onChange, string text = null, string tooltip = null)
		{
			return TextBox(out TextBox outTextBox, onChange, text, tooltip);
		}

		public static TextBox TextBox(out TextBox outTextBox, OnTextboxChanged onChange, string placeholderText = null, string text = null, string tooltip = null)
		{
			TextBox textBox = new TextBox();
			if (onChange != null)
				textBox.TextChanged += (a, b) => TextChanged();
			textBox.PlaceholderText = placeholderText;
			textBox.Text = text;
			textBox.ToolTip = tooltip;
			outTextBox = textBox;

			return textBox;





			void TextChanged()
			{
				(string returnedString, bool textValid) = onChange(textBox.Text);
				if (textBox.Text != returnedString)
				{
					int cursorPos = textBox.CaretIndex;
					textBox.Text = returnedString;
					textBox.CaretIndex = cursorPos;
				}

				if (textValid)
					return;

				textBox.BackgroundColor    =  ISINVALID_COLOR;
				Program.HighPriorityUILoop += MoveBackgroundColorToDefault;
			}
			void MoveBackgroundColorToDefault()
			{
				// Should scale by deltaTime but we don't really care atm
				Color goal = currentStyle.defaultInteractableBackground;
				Color c    = MoveTowards(textBox.BackgroundColor, goal, 0.1f);
				if (c.Rb == goal.Rb && c.Gb == goal.Gb && c.Bb == goal.Bb)
					Program.HighPriorityUILoop -= MoveBackgroundColorToDefault;

				textBox.BackgroundColor = c;
			}
		}

		public static Button Button(Action onClick, string label = null, string tooltip = null)
		{
			return Button(out Button button, onClick, label, tooltip);
		}
		public static Button Button(out Button button, Action onClick, string label = null, string tooltip = null)
		{
			button = new Button();
			if (label != null)
				button.Text = label;
			if(onClick != null)
				button.Click += delegate { onClick(); };
			if(tooltip != null)
				button.ToolTip = tooltip;
			return button;
		}



		public static RadioButtonList RadioButtonList<T>(Action<T> selectionChanged, T initialSelection, Func<T, string> Label, params T[] values)
		{
			Label = Label ?? (arg => arg.ToString());

			RadioButtonList rbList = new RadioButtonList();
			int selectedIndex = 0;
			int i = 0;
			foreach (T value in values)
			{
				rbList.Items.Add( new GenericListItem<T>() { referencedObject = value, text = Label(value)} );
				if (initialSelection.Equals(value))
					selectedIndex = i;
				i++;
			}
			rbList.SelectedIndex = selectedIndex;

			if(selectionChanged != null)
				rbList.SelectedIndexChanged += delegate { selectionChanged(((GenericListItem<T>) rbList.SelectedValue).referencedObject); };
			return rbList;
		}
		public static RadioButtonList RadioButtonList(Action<object> selectionChanged, object initialSelection, Func<object, string> Label, params object[] values)
		{
			Label = Label ?? (arg => arg.ToString());

			RadioButtonList rbList        = new RadioButtonList();
			int             selectedIndex = 0;
			int             i             = 0;
			foreach (object value in values)
			{
				rbList.Items.Add( new GenericListItem<object>() { referencedObject = value, text = Label(value)} );
				if (initialSelection.Equals(value))
					selectedIndex = i;
				i++;
			}
			rbList.SelectedIndex = selectedIndex;

			if(selectionChanged != null)
				rbList.SelectedIndexChanged += delegate { selectionChanged(((GenericListItem<object>) rbList.SelectedValue).referencedObject); };
			return rbList;
		}
		public static ListBox ListBox<T>(out GenericListBox<T> listBox, Action<T> selectionChanged = null, Func<T, string> label = null, params T[] values)
		{
			ListBox lb = new ListBox();
			GenericListBox<T> glb = new GenericListBox<T>(lb, label);
			glb.AddItems(values);
			if(selectionChanged != null)
				glb.SelectionChanged += selectionChanged;
			listBox = glb;
			return lb;
		}





		public static Control[] EnumFlagsToggle<T>(T defaultValue, Func<T, bool, T> onChange, Func<T, string> label = null) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException("T must be an enumerated type");

			label = label ?? (arg => arg.ToString());

			T[] values = (T[]) Enum.GetValues(typeof(T));

			int index = 0;
			dynamic defaultValueLong = Convert.ChangeType(defaultValue, defaultValue.GetTypeCode());
			CheckBox[] outputToggles = new CheckBox[values.Length];
			TypeCode typeCode = defaultValue.GetTypeCode();

			foreach (T value in values)
			{
				dynamic valueLong = Convert.ChangeType(value, defaultValue.GetTypeCode());
				CheckBox box = new CheckBox
				{
					ThreeState = false,
					Checked    = valueLong == 0 ? defaultValueLong == 0 : (defaultValueLong & valueLong) == valueLong,
					Text       = label(value)
				};
				if (onChange != null)
				{
					box.CheckedChanged += delegate
					{
						T changedMask = onChange( value, box.Checked ?? true );
						dynamic changedMaskDynamic = Convert.ChangeType(changedMask, typeCode);
						for (int i = 0; i < values.Length; i++)
						{
							dynamic valueForToggle = Convert.ChangeType(values[i], typeCode);
							bool    isChecked      = valueForToggle == 0 ? changedMaskDynamic == 0 : (changedMaskDynamic & valueForToggle) == valueForToggle;

							if (outputToggles[i].Checked != isChecked)
								outputToggles[i].Checked = isChecked;
						}
					};
				}
				outputToggles[index++] = box;
			}

			return outputToggles;
		}

		public static Control[] EnumFlagsToggle(object defaultValue, Func<object, bool, object> onChange, Func<object, string> label = null)
		{
			Type type = defaultValue.GetType();
			if ( ! type.IsEnum)
				throw new ArgumentException("T must be an enumerated type");

			label = label ?? (arg => arg.ToString());

			Array values = Enum.GetValues(type);

			int        index            = 0;
			TypeCode   typeCode         = Type.GetTypeCode(type);
			dynamic    defaultValueLong = Convert.ChangeType(defaultValue, typeCode);
			CheckBox[] outputToggles    = new CheckBox[values.Length];

			foreach (object value in values)
			{
				dynamic valueLong = Convert.ChangeType(value, typeCode);
				CheckBox box = new CheckBox
				{
					ThreeState = false,
					Checked    = valueLong == 0 ? defaultValueLong == 0 : (defaultValueLong & valueLong) == valueLong,
					Text       = label(value)
				};
				if (onChange != null)
				{
					box.CheckedChanged += delegate
					{
						object  changedMask        = onChange( value, box.Checked ?? true );
						dynamic changedMaskDynamic = Convert.ChangeType(changedMask, typeCode);
						for (int i = 0; i < values.Length; i++)
						{
							dynamic valueForToggle = Convert.ChangeType(values.GetValue(i), typeCode);
							bool    isChecked      = valueForToggle == 0 ? changedMaskDynamic == 0 : (changedMaskDynamic & valueForToggle) == valueForToggle;

							if (outputToggles[i].Checked != isChecked)
								outputToggles[i].Checked = isChecked;
						}
					};
				}
				outputToggles[index++] = box;
			}

			return outputToggles;
		}







		public static CheckBox Toggle(bool defaultValue, Action<bool> onToggle = null, string label = null)
		{
			CheckBox checkBox = new CheckBox
			{
				ThreeState = false,
				Checked    = defaultValue
			};
			if (label != null)
				checkBox.Text = label;
			if(onToggle != null)
				checkBox.CheckedChanged += delegate { onToggle( checkBox.Checked ?? true  ); };
			return checkBox;
		}




		public static ImageView ImageView(out ImageView outImage)
		{
			outImage = new ImageView();
			return outImage;
		}








		public static void PlaceUnderMouse(Form form)
		{
			form.Location = new Point((int)HardwareInterface.MousePostion.x - form.ClientSize.Width / 2, (int)HardwareInterface.MousePostion.y - form.ClientSize.Height / 2) ;
		}




		public static Control[] AutoGenerateUIFieldsFor(object o, out Action[] actionToUpdateFields, bool attributeOnly)
		{
			Type type = (o is Type t ? t : o.GetType());
			List<Control> output = new List<Control>();
			List<Action> updateFieldsTemp = new List<Action>();


			IEnumerable<(MemberInfo member, AutoUIAttribute attribute)> members;
			if (attributeOnly)
			{
				members = from m in type.GetMembers(BindingFlags.Public | BindingFlags.Static)
				          let attr = m.GetCustomAttribute<AutoUIAttribute>()
				          where attr != null
				          orderby attr.order
				          select (m, attr);
			}
			else
			{
				members = from m in type.GetMembers(BindingFlags.Public | BindingFlags.Static)
				          let attr = m.GetCustomAttribute<AutoUIAttribute>()
				          orderby attr ? .order ?? m.MetadataToken
				          select (m, attr);
			}



			foreach (var memberInfoTuple in members)
			{
				MemberInfo member = memberInfoTuple.member;
				string name = UserFriendlyString(member.Name);
				switch (member)
				{
					case FieldInfo field:
					{
						Control ctrl = GenerateUIForField(name, field.FieldType, () => field.GetValue(o), (v) => field.SetValue(o, v), out Action a);
						if (ctrl == null)
							continue;

						output.Add(ctrl);
						if(a != null)
							updateFieldsTemp.Add(a);
						break;
					}
					case PropertyInfo property:
					{
						MethodInfo   getMethod = property.GetGetMethod();
						MethodInfo   setMethod = property.GetSetMethod();
						Func<object> get       = null;
						if (getMethod != null)
							get = () => getMethod.Invoke(o, null);
						Action<object> set = null;
						if (setMethod != null)
							set = (newValue) => setMethod.Invoke(o, new []{newValue});

						Control ctrl = GenerateUIForField(name, property.PropertyType, get, set, out Action a);
						if (ctrl == null)
							continue;

						output.Add(ctrl);
						if(a != null)
							updateFieldsTemp.Add(a);
						break;
					}
					case MethodInfo methodInfo when methodInfo.GetParameters().Length > 0 || methodInfo.ReturnType != typeof(void):
						continue;
					case MethodInfo methodInfo:
						output.Add( Button( () => methodInfo.Invoke( o, null ), name) );
						break;
				}
			}

			actionToUpdateFields = updateFieldsTemp.ToArray();
			return output.ToArray();
		}


		public static Control GenerateUIForField(string label, Type type, Func<object> get, Action<object> set, out Action updateField)
		{
			object value = get();
			if (value == null)
			{
				if (type == typeof(string))
					value = string.Empty;
			}
			switch (value)
			{
				case float v:
				{
					TextBox tb = TextBox(ParseFloatAndSet, v.ToString(CultureInfo.InvariantCulture));
					updateField = () => tb.Text = ((float)get()).ToString(CultureInfo.InvariantCulture);

					if (string.IsNullOrEmpty(label))
						return tb;
					else
						return Horizontal(
						                  Label(label),
						                  tb
						                 );
				}
				case int v:
				{
					TextBox tb = TextBox(ParseIntAndSet, v.ToString(CultureInfo.InvariantCulture));
					updateField = () => tb.Text = ((int)get()).ToString();

					if (string.IsNullOrEmpty(label))
						return tb;
					else
					{
						return Horizontal(
						                  Label(label),
						                  tb
						                 );
					}
				}
				case bool v:
				{
					CheckBox toggle = Toggle(v, ToggleSet);
					updateField = () => toggle.Checked = (bool) get();
					return Horizontal(Label(label), toggle);
				}
				case Enum v:
				{
					updateField = null;
					Array enumValues = GetEnumValuesAsArray(v.GetType());
					object[] objValues = new object[enumValues.Length];
					for (int i = 0; i < enumValues.Length; i++)
					{
						objValues[i] = enumValues.GetValue(i);
					}
					bool isFlags = v.GetType().IsDefined(typeof(FlagsAttribute), false);

					Control ctrl;
					if (isFlags)
						ctrl = Horizontal(EnumFlagsToggle((object) v, EnumMaskChanged));
					else
						ctrl = RadioButtonList( EnumChanged, v, null, objValues );

					return Horizontal(Label(label), ctrl);
				}
				case string v:
				{
					if (set != null)
					{
						TextBox t = TextBox(ParseStringAndSet, v.ToString(CultureInfo.InvariantCulture));
						updateField = () => t.Text = (string)get();
						return t;
					}
					// Lookup field
					else
					{
						Label(out Label l, v);
						updateField = () => l.Text = (string)get();
						return l;
					}
				}
				default:
					d.logerror($"UI not implemented for {label} of type {value ? .GetType()}");
					updateField = null;
					return null;
			}



			(string newText, bool textValid) ParseFloatAndSet(string textboxString)
			{
				if (float.TryParse(textboxString, out float parsedValue))
				{
					bool valid = true;
					float boundValue = (float) get();
					if (parsedValue != boundValue)
					{
						set(parsedValue);
						// Fetch value back to find out if the value we set was changed by the property's function
						float getSetValueBack = (float) get();
						if (parsedValue != getSetValueBack)
						{
							valid = false;
							textboxString = getSetValueBack.ToString(CultureInfo.InvariantCulture);
						}
					}
					return (textboxString, valid);
				}
				else
					return (new string(textboxString.Where(c => char.IsDigit(c) || c == '.' || c == ',').ToArray()) , false);
			}
			(string newText, bool textValid) ParseIntAndSet(string textboxString)
			{
				if (int.TryParse(textboxString, out int parsedValue))
				{
					bool valid = true;
					int boundValue = (int) get();
					if (parsedValue != boundValue)
					{
						set(parsedValue);
						// Fetch value back to find out if the value we set was changed by the property's function
						int getSetValueBack = (int) get();
						if (parsedValue != getSetValueBack)
						{
							valid         = false;
							textboxString = getSetValueBack.ToString();
						}
					}
					return (textboxString, valid);
				}
				else
					return (new string(textboxString.Where(char.IsDigit).ToArray()) , false);
			}
			(string newText, bool textValid) ParseStringAndSet(string textboxString)
			{
				set(textboxString);
				return (textboxString, true);
			}

			void ToggleSet(bool uiBool)
			{
				set(uiBool);
			}
			void EnumChanged(object enumValue)
			{
				set(enumValue);
			}

			object EnumMaskChanged(object enumValue, bool inclusion)
			{
				Type enumType = enumValue.GetType();
				TypeCode typeCode = Type.GetTypeCode(enumType);
				d.log(typeCode);
				dynamic newValue = Convert.ChangeType(enumValue, typeCode);
				dynamic currentValue = Convert.ChangeType(get(), typeCode);
				dynamic outValue = currentValue;

				if (newValue == 0)
				{
					// if our toggle sets zero, zero out all the other flags
					if(inclusion)
						outValue = 0; // for clarity's sake
					// if our toggle unset zero, enable all the other flags
					else
						outValue = ~0;
				}
				// If its a standard non zero flag, include / exclude that specific flag
				else if (inclusion)
					outValue |= newValue;
				else
					outValue &= ~newValue;
				set(outValue);
				return get();
			}
		}


		public static T[] GetEnumValuesAsArray<T>()
		{
			return (T[])Enum.GetValues(typeof(T));
		}
		public static Array GetEnumValuesAsArray(Type t)
		{
			return Enum.GetValues(t);
		}


		public class GenericListBox<T>
		{
			public bool Visible
			{
				get => host.Visible;
				set => host.Visible = value;
			}

			readonly ListBox host;
			readonly Func<T, string> toString;
			public event Action<T> SelectionChanged;
			public GenericListBox(ListBox source, Func<T, string> toStringFunc)
			{
				host = source;
				toString = toStringFunc ?? (arg => arg.ToString());
				source.SelectedIndexChanged += delegate
				{
					if(GetSelection(out T selection))
						SelectionChanged ? .Invoke( selection );
				};
			}


			public void Clear()
			{
				host.Items.Clear();
			}


			public void AddItem(T item)
			{
				host.Items.Add(new GenericListItem<T>() { referencedObject = item, text = toString(item) });
			}


			public void AddItems(params T[] items)
			{
				GenericListItem<T>[] useableItems = new GenericListItem<T>[items.Length];
				for (int i = 0; i < items.Length; i++)
				{
					useableItems[i] = new GenericListItem<T>() { referencedObject = items[i], text = toString(items[i]) };
				}
				host.Items.AddRange(useableItems);
			}


			public void AddItems(IEnumerable<T> items)
			{
				List<GenericListItem<T>> useableItems = new List<GenericListItem<T>>();
				foreach (T item in items)
				{
					useableItems.Add(new GenericListItem<T>() { referencedObject = item, text = toString(item) });
				}
				host.Items.AddRange(useableItems);
			}


			public void SetSelection(int index)
			{
				host.SelectedIndex = index;
			}



			public bool GetSelection(out T selection)
			{
				if (host.SelectedValue == null)
				{
					selection = default(T);
					return false;
				}
				else
				{
					selection = ((GenericListItem<T>) host.SelectedValue).referencedObject;
					return  true;
				}
			}
		}

		class GenericListItem<T> : Eto.Forms.IListItem
		{
			public string text;
			public string Text { get => text; set => text = value; }
			public string Key  { get; }
			public T      referencedObject;
		}


		public delegate (string newText, bool textValid) OnTextboxChanged(string textboxString);





		static Color MoveTowards(Color current, Color goal, float step)
		{
			return new Color(MoveTowards(current.R, goal.R, step),
			                 MoveTowards(current.G, goal.G, step),
			                 MoveTowards(current.B, goal.B, step));
		}
		static float MoveTowards(float current, float goal, float step)
		{
			float delta = goal - current;
			if (Math.Abs(delta) > Math.Abs(step))
				delta = delta < 0 ? -Math.Abs(step) : Math.Abs(step);
			return current + delta;
		}


		/// <summary>
		/// Transforms a string, ex : DSATestStringOE_A -> DSA Test String OE A
		/// </summary>
		/// <param name="baseString"></param>
		/// <returns></returns>
		static string UserFriendlyString(this string baseString)
		{
			if(string.IsNullOrEmpty(baseString))
				return string.Empty;
			char[] charArray = baseString.ToCharArray();


			string s = char.ToUpper(charArray[0]).ToString();

			// skip first char since we processed it above
			for (int i = 1; i < charArray.Length; i++)
			{
				char c = charArray[i];

				// If not last char'
				if (i + 1 < charArray.Length)
				{
					if(c == '_')
					{
						s += ' ';
						continue;
					}

					char nxtC = charArray[i + 1];
					// DSATestStringOEA -> DSATest String OE_A
					if (char.ToLower(c) == c && char.ToUpper(nxtC) == nxtC)
					{
						s += c + " ";
						continue;
					}

					if (i != 0)
					{
						char lstC = charArray[i - 1];
						// DSATestStringOEA -> DSA TestStringOE_A
						if (char.ToUpper(c) == c && char.ToLower(nxtC) == nxtC && char.ToUpper(lstC) == lstC)
							s += " ";
					}

				}
				s += c;
			}
			return s;
		}



		public class AutoUIAttribute : Attribute
		{
			public readonly int order;

			/// <summary>
			///
			/// </summary>
			/// <param name="newOrder">Is by default the current line number</param>
			public AutoUIAttribute([CallerLineNumber] int newOrder = 0)
			{
				this.order = newOrder;
			}
		}
	}
}