﻿using System;
using System.Collections.Generic;
using Eto.Drawing;
using Eto.Forms;








namespace InputForwarder.Windows
{
	public class SingletonWindow : Form
	{
		static readonly Dictionary<Type, SingletonWindow> SINGLETONS = new Dictionary<Type, SingletonWindow>();


		protected SingletonWindow((int x, int y) size, Action onClose = null)
		{
			Type type = this.GetType();
			if (SINGLETONS.ContainsKey(type))
				SINGLETONS[type] ? .Close();
			else
				SINGLETONS.Add(type, null);
			SINGLETONS[type] = this;


			this.Title = type.Name;
			this.ClientSize = new Size(size.x, size.y);



			this.Closing += (o, e) =>
			{
				if (SINGLETONS[type] == this)
					SINGLETONS[type] = null;
			};
			if (onClose != null)
				this.Closed += (o, e) => onClose();

			this.Show();
			UILayouting.PlaceUnderMouse(this);

			if(this.GetType() != typeof(MainWindow))
				Program.MainWindow.Closing += delegate { this.Close(); };
		}


		public static bool TryGetOpenedInstance<T>(out T instance) where T : SingletonWindow
		{
			if (SINGLETONS.TryGetValue(typeof(T), out SingletonWindow baseInstance))
			{
				instance = (T) baseInstance;
				return true;
			}
			else
			{
				instance = null;
				return false;
			}
		}
	}
}