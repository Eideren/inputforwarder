﻿using System;
using static InputForwarder.UILayouting;







namespace InputForwarder.Windows
{
	public class SettingsWindow : SingletonWindow
	{
		readonly Action[] actionToUpdateFields;
		public SettingsWindow(Action onClose = null) : base((480, 240), onClose)
		{
			Content = Vertical( 3,
			                     AutoGenerateUIFieldsFor(typeof(Settings), out actionToUpdateFields, true)
			                     );

			Program.LowPriorityUILoop += OnUILoop;
			this.Closing              += delegate { Program.LowPriorityUILoop -= OnUILoop; };
		}


		void OnUILoop()
		{
			foreach (Action action in actionToUpdateFields)
			{
				action();
			}
		}
	}
}