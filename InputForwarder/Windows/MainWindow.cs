﻿using System;
using System.Collections.Generic;
using System.Linq;
using ENet;
using InputForwarder.Windows;
using LiteNetLib;
using static InputForwarder.UILayouting;


using Eto.Forms;








namespace InputForwarder
{
	public class MainWindow : SingletonWindow
	{
		readonly Label debugLabel, targetLabel;
		readonly GenericListBox<InputTarget> inputTargetSelector;

		readonly TextBox ipField, portField;
		readonly Button hostButton, joinButton, disconnectButton;
		readonly List<InputTarget> uiTargets;

		public MainWindow(Action onClose) : base((400, 200), onClose)
		{

			//this.Closing += delegate { Program. };

			// LAYOUT
			Content =
				Horizontal( 3,
					Vertical(
						Label( out debugLabel, string.Empty ),
						Horizontal(
							TextBox( out ipField, null, "IP Address", "127.0.0.1", "IP Address" ),
							TextBox( out portField, OnPortChanged, "Port", "5858", "Port" )
						),
						Horizontal(
							Button( out hostButton, OnButtonHost, "Host", "Start a session, other users will be able to join you and send their inputs over" ),
							Button( out joinButton, OnButtonJoin, "Join", "Join the session at the ip and port provided above to send your inputs to" ),
							Button( out disconnectButton, OnButtonDisconnect, "Disconnect" )
						)
					),
					Vertical(
						Label( out targetLabel, nameof( targetLabel ) ),
						ListBox( out inputTargetSelector, UpdateInputTarget ),
						Horizontal(
							Button( delegate { new GamepadManagementWindow(); }, "Gamepads" ),
							Button( delegate { new SettingsWindow(); }, "Settings" )
						)
					)
				);
			disconnectButton.Visible = false;
			uiTargets = new List<InputTarget>();

			Program.LowPriorityUILoop += () =>
			{
				UpdateProcessesInUI();
				ShowNetworkInfo();
			};

			NetworkProgramLayer.OnSwitching += delegate { Program.CallOnceOverUIThread(OnSwitchingState); };
			NetworkProgramLayer.OnIdle += delegate { Program.CallOnceOverUIThread(OnNetworkIdle); };
			NetworkProgramLayer.OnHost += delegate { Program.CallOnceOverUIThread(OnIsHosting); };
			NetworkProgramLayer.OnClient += delegate { Program.CallOnceOverUIThread(OnIsClient); };



			(string newText, bool textValid) OnPortChanged(string textboxString)
			{
				return ( newText:textboxString, textValid: textboxString.All(char.IsDigit) );
			}
			void UpdateInputTarget(InputTarget newInputTarget)
			{
				Program.currentInputTarget = newInputTarget;
			}
		}


		void OnIsClient ()
		{
			disconnectButton.Visible = true;

			inputTargetSelector.Visible = false;
			targetLabel.Visible = false;
			hostButton.Visible  = false;
			joinButton.Visible  = false;
			ipField.Visible     = false;
			portField.Visible   = false;
		}


		void OnIsHosting ()
		{
			inputTargetSelector.Visible = true;
			targetLabel.Visible         = true;
			disconnectButton.Visible = true;

			hostButton.Visible         = false;
			joinButton.Visible         = false;
			ipField.Visible            = false;
			portField.Visible          = false;
		}


		void OnNetworkIdle ()
		{
			disconnectButton.Visible = false;

			inputTargetSelector.Visible = true;
			targetLabel.Visible = true;
			hostButton.Visible  = true;
			joinButton.Visible  = true;
			ipField.Visible     = true;
			portField.Visible   = true;
		}


		void OnSwitchingState ()
		{
			disconnectButton.Visible = true;

			inputTargetSelector.Visible = false;
			targetLabel.Visible         = false;
			hostButton.Visible          = false;
			joinButton.Visible          = false;
			ipField.Visible             = false;
			portField.Visible           = false;
		}


		void OnButtonDisconnect()
		{
			NetworkProgramLayer.Stop("User action");
		}

		void OnButtonHost ()
		{
			if (ushort.TryParse(portField.Text, out ushort port) == false)
			{
				debugLabel.Text = "Invalid port";
				return;
			}
			debugLabel.Text = $"Hosting";
			NetworkProgramLayer.Host(port);
		}
		async void OnButtonJoin ()
		{
			if (ushort.TryParse(portField.Text, out ushort port) == false)
			{
				debugLabel.Text = "Invalid port";
				return;
			}
			string ip = ipField.Text;
			debugLabel.Text = $"Joining {ip} : {port}";
			ConnectionTask.Result result = await NetworkProgramLayer.Join(ip, port);
			switch (result)
			{
				case ConnectionTask.Result.AlreadyConnected:
					debugLabel.Text = $"Already Connected";
					break;
				case ConnectionTask.Result.Failed:
					debugLabel.Text = $"Connection failed";
					break;
				case ConnectionTask.Result.Successful:
					debugLabel.Text = $"Connection Successful";
					break;
				case ConnectionTask.Result.Waiting:
					debugLabel.Text = $"Connection waiting?";
					break;
				default:
					throw new NotImplementedException();
			}
		}


		static readonly List<InputTarget> BASE_TARGETS = new List<InputTarget> {InputTarget.None, InputTarget.Any};
		void UpdateProcessesInUI()
		{
			bool kbmEnabled = Program.currentInputTarget.SimulateInputAllowed(Program.activeWindow);
			targetLabel.Text = $"Client keyboard and mouse target:\n({(kbmEnabled ? "Target in focus:peer kb+m allowed" : "Target not in focus:peer kb+m denied")})";
			// no infinite for loop in here, GDK is calling this function continuously itself
			if(BASE_TARGETS.Count > 2)
				BASE_TARGETS.RemoveRange(2, BASE_TARGETS.Count - 2);

			// copy it to avoid most concurrence, it isn't that important anyway
			lock (Program.PROCESSLOCK)
			{
				for (int i = 0; i < Program.FILTERED_PROCESSES.Count; i++)
				{
					BASE_TARGETS.Add(new InputTarget(Program.FILTERED_PROCESSES[i]));
				}
			}

			if(DataMatchView(BASE_TARGETS) == false)
				ReplaceViewData(BASE_TARGETS, Program.currentInputTarget);
		}


		static readonly List<NetPeer> buff = new List<NetPeer>();
		void ShowNetworkInfo()
		{
			if (NetworkProgramLayer.networkState == NetworkProgramLayer.NetworkMode.Idle)
			{
				debugLabel.Text = "";
				return;
			}
			Network.instance ? .netTransport.GetPeersNonAlloc(buff, ConnectionState.Connected);
			string peers = string.Empty;
			for (int i = 0; i < buff.Count; i++)
			{
				if (i != 0)
					peers += $", {buff[i].EndPoint}";
				else
					peers += buff[i].EndPoint;
			}

			Network.Info.FetchLatencyInfo(out double latencyAverage, out int latencyMax);
			debugLabel.Text = $"Network Status:\nIs {NetworkProgramLayer.networkState}\nRTT: ~{latencyAverage:F} ^{latencyMax}\nConnected clients :{peers}";
		}










		bool DataMatchView(IList<InputTarget> srcEnum)
		{
			using (IEnumerator<InputTarget> src = srcEnum.GetEnumerator(), dst = uiTargets.GetEnumerator())
			{
				while ( true )
				{
					bool srcHas = src.MoveNext();
					bool dstHas = dst.MoveNext();

					if (!srcHas && !dstHas)
						return true;
					// Count or items doesn't match
					if (srcHas != dstHas)
						return false;

					if (dst.Current != src.Current)
						return false;
				}
			}
		}


		void ReplaceViewData(IEnumerable<InputTarget> srcEnum, InputTarget selection)
		{
			uiTargets.Clear();
			uiTargets.AddRange(srcEnum);
			inputTargetSelector.Clear();
			int index = 0;
			int selectionIndex = 0;
			foreach (InputTarget inputTarget in srcEnum)
			{
				if (inputTarget == selection)
					selectionIndex = index;
				index++;
			}
			inputTargetSelector.AddItems(srcEnum);
			inputTargetSelector.SetSelection(selectionIndex);
		}
	}






}