﻿using System;
using System.Threading;
using System.Threading.Tasks;

using InputForwarder.Hardware;
using InputForwarder.Windows;
using static InputForwarder.UILayouting;

namespace InputForwarder
{
	public class PeerGamepadAssignmentModal : SingletonWindow
	{
		readonly GenericListBox<GamepadSource> sourceSelection;
		bool result = false;
		bool ack = false;
		int slot;





		public static async Task<(bool ok, GamepadSource source)> New(int slotIndex)
		{
			PeerGamepadAssignmentModal modal = new PeerGamepadAssignmentModal
			{
				slot = slotIndex,
			};
			Func<(bool, GamepadSource)> waitForModalAck = modal.WaitForModalAck;
			return await Task.Run( waitForModalAck );
		}


		(bool ok, GamepadSource peer) WaitForModalAck()
		{
			while(ack == false)
				Thread.Sleep(50);

			GamepadSource selectedPeer = null;

			bool free = false;
			Program.CallOnceOverUIThread( delegate
			{
				result = result && sourceSelection.GetSelection(out selectedPeer);
				this.Close();
				free = true;
			} );

			while (free == false)
			{
				Thread.Sleep(16);
			}
			return (result, selectedPeer);
		}


		PeerGamepadAssignmentModal() : base((300, 300), null)
		{
			Content = 
				Vertical( 3,
					ListBox( out sourceSelection, label: source => source.ToString() ),
					Horizontal(
						Button( OnOk, "Ok" ),
						Button( OnCancel, "Cancel" )
					)
				);

			sourceSelection.AddItems( GamepadSource.ExistingSources() );
		}




		void OnCancel()
		{
			ack = true;
			result = false;
		}


		void OnOk()
		{
			ack = true;
			result = true;
		}
	}
}