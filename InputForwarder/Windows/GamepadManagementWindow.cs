﻿namespace InputForwarder
{
	using System.Linq;
	using Eto.Forms;
	using ENet;
	using Windows;
	using Hardware;
	using static UILayouting;



	public class GamepadManagementWindow : SingletonWindow
	{
		readonly Label[] debugLabels;

		public GamepadManagementWindow(System.Action onClose = null) : base((340, 300), onClose)
		{
			// LAYOUT
			debugLabels = new Label[HardwareInterface.GAMEPADS.Length];

			Control[] gamepadsUI = new Control[HardwareInterface.GAMEPADS.Length];
			for (int i = 0; i < gamepadsUI.Length; i++)
			{
				gamepadsUI[i] = GamepadLayout(i);
			}
			Content = Vertical(gamepadsUI);

			Program.LowPriorityUILoop += UpdateGamepadInfo;
		}


		void UpdateGamepadInfo()
		{
			var mappings = GamepadMappings.SampleRemoteMappings();
			for (int i = 0; i < HardwareInterface.GAMEPADS.Length; i++)
			{
				var state = GamepadMappings.SampleStateFor( i );
				
				var mappingToString = string.Join(", ", 
					from m in mappings
					where m.indexMappedTo == i
					select m.source.ToString());

				string str;
				if(NetworkProgramLayer.IsHost)
					str = $"Simulated Gamepad #{i}";
				else
					str = $"Send Gamepad #{i}?";
				if(string.IsNullOrWhiteSpace(mappingToString) == false)
					str += $"\nControlled by {mappingToString}\n{state.Print()}";
				debugLabels[ i ].Text = str;
			}
		}


		Control GamepadLayout(int gamepadIndex)
		{
			Control widget = Horizontal( 3,
			                            Label(out debugLabels[gamepadIndex]),
			                            Vertical(
			                                     Button(() => OnClickAssignSlot(gamepadIndex), "Assign to ...", "Add a source gamepad to control this simulated gamepad"),
			                                     Button(() => OnClickClear(gamepadIndex), "Clear", "Remove all sources controlling this gamepad")
			                                     )
			                           );
			return widget;
		}

		
		
		async void OnClickAssignSlot(int index)
		{
			(bool ok, GamepadSource gamepad) result = await PeerGamepadAssignmentModal.New(index);
			if (result.ok)
				GamepadMappings.TryAddMapping(result.gamepad, index);
		}
		void OnClickClear(int index)
		{
			GamepadMappings.RemoveMappings( x => x.index == index );
		}
	}
}