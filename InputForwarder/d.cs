﻿// ReSharper disable InconsistentNaming

using System.Runtime.CompilerServices;
using Console = System.Console;
using ConsoleColor = System.ConsoleColor;

namespace InputForwarder
{
	/// <summary>
	/// Shortcut and extension of the Console class
	/// </summary>
	public static class d
	{
		/// <summary>
		/// Shortcut for Console.WriteLine
		/// </summary>
		public static void log<T>(T str)
		{
			Console.ResetColor();
			Console.WriteLine(str ? .ToString());
		}
		/// <summary>
		/// Shortcut for Console.WriteLine
		/// </summary>
		public static void logwarning<T>(T str)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine(str ? .ToString());
		}
		/// <summary>
		/// Shortcut for Console.Error.WriteLine
		/// </summary>
		public static void logerror<T>(T str)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Error.WriteLine(str ? .ToString());
		}

		/// <summary>
		/// Prints to the console the line number and the function that called it, usefull to debug with messed up stacktraces
		/// </summary>
		public static void line(string message = null, [CallerLineNumber] int line = 0, [CallerMemberName] string member = null, [CallerFilePath] string filePath = null)
		{
			Console.ForegroundColor = ConsoleColor.Blue;
			Console.WriteLine($"{filePath} - {member} @ {line} : {message}");
		}
	}
}