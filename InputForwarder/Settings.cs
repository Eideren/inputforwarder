﻿// Fields in this class are accessed from reflection
// ReSharper disable ConvertToConstant.Global
// ReSharper disable FieldCanBeMadeReadOnly.Global

using System;
using AutoUIAttribute = InputForwarder.UILayouting.AutoUIAttribute;








namespace InputForwarder
{
	public static class Settings
	{
		[AutoUI] public static MouseMovement mouseMovement = MouseMovement.Absolute;
		static int _hardwarePollingRate = 120;
		[AutoUI] public static int HardwarePollingRate
		{
			get => _hardwarePollingRate;
			set => _hardwarePollingRate = Math.Max(value, 0);
		}

		[AutoUI] public static bool LogDeviceChangesToTerminal
		{
			get => Program.logDeviceChangesToTerminal;
			set => Program.logDeviceChangesToTerminal = value;
		}




		public enum MouseMovement
		{
			Relative,
			Absolute,
			Disabled
		}
	}
}