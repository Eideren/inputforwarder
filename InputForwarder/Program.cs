using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using ThreadState = System.Diagnostics.ThreadState;

using Eto.Forms;

using EMath;
using ENet;

using SimWinInput;
using WindowsInput.Native;
using Eto.Drawing;
using InputForwarder.Hardware;

using Exception = System.Exception;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using NotImplementedException = System.NotImplementedException;

using AppDomain = System.AppDomain;
using IntPtr = System.IntPtr;
using Console = System.Console;

using DIKey = SharpDX.DirectInput.Key;








namespace InputForwarder
{
	using SharpDX.XInput;



	public static class Program
	{
		public const int UI_LOOP_MS = 100;
		public const int UI_LOOP_HIGHPRIORITY_MS = 16;
		public const int LOGIC_LOOP_MS = 16;





		public static MainWindow MainWindow { get; private set; }
		static Thread logicLoop;

		public static event System.Action<ExitMotif> OnApplicationQuit;
		public static event System.Action LowPriorityUILoop, HighPriorityUILoop, LogicLoop;





		public static InputTarget currentInputTarget = InputTarget.None;
		public static Process activeWindow;
		public static readonly List<Process> FILTERED_PROCESSES = new List<Process>();
		public static readonly object PROCESSLOCK = new object();
		public static bool monitoring;

		static Application uiApp;




		static bool _logDeviceChangesToTerminal = false;
		public static bool logDeviceChangesToTerminal
		{
			get => _logDeviceChangesToTerminal;
			set
			{
				if(_logDeviceChangesToTerminal == value)
					return;
				_logDeviceChangesToTerminal = value;
				LogDeviceChangesToTerminal(_logDeviceChangesToTerminal);
			}
		}


		static Program()
		{
			AppDomain.CurrentDomain.AssemblyResolve += delegate(object sender, ResolveEventArgs rea)
			{
				bool unsafeLoad = false;
				string assemblyFile = (rea.Name.Contains(","))
					                      ? rea.Name.Substring(0, rea.Name.IndexOf(','))
					                      : rea.Name;
				if (assemblyFile == "LiteNetLib")
					unsafeLoad = true;

				assemblyFile += ".dll";
				string targetPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "lib", assemblyFile);
				//d.log($"{targetPath} ? {File.Exists(targetPath)}");
				try
				{
					Assembly ass = unsafeLoad ? Assembly.UnsafeLoadFrom(targetPath) : Assembly.LoadFile(targetPath);
					//d.log(ass);
					return ass;
				}
				catch (Exception ex)
				{
					if (ex is FileNotFoundException)
						return null;
					else
					{
						d.logerror(ex);
						return null;
					}
				}
			};
		}




		[STAThread]
		static void Main (string[] args)
		{
			try
			{
				//AppDomain.CurrentDomain.FirstChanceException += (sender, e) => UnhandledException(e.Exception);
				AppDomain.CurrentDomain.UnhandledException += (sender, e) => UnhandledException(e.ExceptionObject);

				logicLoop = new Thread(() =>
				{
					while (exiting == false)
					{
						Thread.Sleep(LOGIC_LOOP_MS);
						LogicLoop ? .Invoke();
					}
				});
				logicLoop.Start();




				#region BASIC_LOGIC_DELEGATES
				LogicLoop += MonitorProcesses;
				NetworkProgramLayer.OnClient += () =>
				{
					// Add a default mapping on the first connected pad when none were set
					if( GamepadMappings.SampleRemoteMappings().Count == 0 )
					{
						foreach( var localSource in HardwareInterface.GAMEPADS )
						{
							if( localSource.IsConnected )
							{
								GamepadMappings.TryAddMapping( localSource, 0 );
							}
						}
					}

					//HardwareInterface.KEYBOARD_STATE.OnKeyChange += EmulateGamepadWithKeyboard;
					
					HardwareInterface.KEYBOARD_STATE.OnKeyChange += SendKeyboardState;

					HardwareInterface.MOUSE_STATE.OnAxisChange += SendMouseAxisChange;
					HardwareInterface.MOUSE_STATE.OnButtonChange += SendMouseButtonChange;

					GamepadMappings.GamepadStateChanged += SendGamepadState;
				};
				NetworkProgramLayer.OnHost += () =>
				{
					//HardwareInterface.KEYBOARD_STATE.OnKeyChange += EmulateGamepadWithKeyboard;
					HardwareInterface.KEYBOARD_STATE.OnKeyChange += SimulateKeyReceived;

					HardwareInterface.MOUSE_STATE.OnButtonChange += SimulateMouseButton;
					HardwareInterface.MOUSE_STATE.OnAxisChange   += SimulateMouseMovement;

					GamepadMappings.GamepadStateChanged += SimulateGamepad;
					GamepadMappings.MappingRemoved += RemovedMappingUnplug;
				};
				NetworkProgramLayer.OnIdle += () =>
				{
					//HardwareInterface.KEYBOARD_STATE.OnKeyChange -= EmulateGamepadWithKeyboard;
					HardwareInterface.KEYBOARD_STATE.OnKeyChange -= SendKeyboardState;
					HardwareInterface.MOUSE_STATE.OnAxisChange   -= SendMouseAxisChange;
					HardwareInterface.MOUSE_STATE.OnButtonChange -= SendMouseButtonChange;
					GamepadMappings.GamepadStateChanged -= SendGamepadState;


					HardwareInterface.KEYBOARD_STATE.OnKeyChange -= SimulateKeyReceived;
					HardwareInterface.MOUSE_STATE.OnAxisChange -= SimulateMouseMovement;
					HardwareInterface.MOUSE_STATE.OnButtonChange -= SimulateMouseButton;
					GamepadMappings.GamepadStateChanged -= SimulateGamepad;
					GamepadMappings.MappingRemoved -= RemovedMappingUnplug;
				};

				// LOCAL FUNCTIONS, used when swapping between network state

				
				
				/*void EmulateGamepadWithKeyboard(DIKey hardwareKey, VirtualKeyCode virtualKeyCode, ButtonEvent arg3)
				{
					var gamepadstate = HardwareInterface.SHARPDX_GAMEPADS[ 0 ];
					switch( hardwareKey )
					{
						case DIKey.NumberPad8: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.Y, arg3); break;
						case DIKey.NumberPad2: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.A, arg3 ); break;
						case DIKey.NumberPad4: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.X, arg3 ); break;
						case DIKey.NumberPad6: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.B, arg3 ); break;
						case DIKey.NumberPad9: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.RightShoulder, arg3 ); break;
						case DIKey.NumberPad7: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.LeftShoulder, arg3 ); break;
						case DIKey.Up: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.DPadUp, arg3 ); break;
						case DIKey.Down: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.DPadDown, arg3 ); break;
						case DIKey.Left: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.DPadLeft, arg3 ); break;
						case DIKey.Right: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.DPadRight, arg3 ); break;
						case DIKey.NumberPadEnter: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.Start, arg3 ); break;
						case DIKey.Backslash: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.Back, arg3 ); break;
						case DIKey.NumberPad5: gamepadstate.Buttons = CondBtn(gamepadstate.Buttons, GamePadControl.Guide, arg3 ); break;
						default: 
							return;
					}
					
					PacketIO.NewGamepadStateCommand(0);

					GamePadControl CondBtn( GamePadControl currentValue, GamePadControl mask, ButtonEvent evt )
					{
						return evt == ButtonEvent.Pressed ? currentValue | mask : currentValue & ~mask;
					}
				}*/

				void SendKeyboardState (DIKey hardwareKey, VirtualKeyCode virtualKeyCode, ButtonEvent arg3)
				{
					PacketIO.NewKeyboardStateCommand();
				}
				void SendGamepadState (Gamepad g, int index)
				{
					PacketIO.NewGamepadStateCommand(g, index);
				}
				void SendMouseAxisChange(float value, MouseAxis axis, bool relative)
				{
					PacketIO.NewMouseAxisCommand();
				}
				void SendMouseButtonChange(int buttonIndex, ButtonEvent buttonEvent)
				{
					PacketIO.NewMouseButtonCommand();
				}
				async void MonitorProcesses()
				{
					if(monitoring)
						return;

					monitoring = true;
					List<Process> temp;
					lock (PROCESSLOCK)
					{
						temp = new List<Process>(FILTERED_PROCESSES);
					}
					await ProcessHelper.FilterProcess(Process.GetProcesses(), temp, true);
					activeWindow = ProcessHelper.GetFocusedProcess(temp);
					lock (PROCESSLOCK)
					{
						FILTERED_PROCESSES.Clear();
						FILTERED_PROCESSES.AddRange(temp);
					}
					monitoring = false;
				}

				void SimulateMouseButton (int buttonIndex, ButtonEvent bEvent)
				{
					if(currentInputTarget.SimulateInputAllowed(activeWindow) == false)
						return;

					switch (bEvent)
					{
						case ButtonEvent.Pressed:
							HardwareInterface.INPUT_SIMULATOR.Mouse.XButtonDown(buttonIndex);
							break;
						case ButtonEvent.Released:
							HardwareInterface.INPUT_SIMULATOR.Mouse.XButtonUp(buttonIndex);
							break;
						default:
							throw new NotImplementedException($"{bEvent}");
					}
				}

				void SimulateMouseMovement (float value, MouseAxis axis, bool relative)
				{
					if(currentInputTarget.SimulateInputAllowed(activeWindow) == false)
						return;

					if (axis == MouseAxis.Depth_Scroll)
					{
						HardwareInterface.INPUT_SIMULATOR.Mouse.VerticalScroll((int) value);
						return;
					}

					if (relative)
					{
						float2 movement2D = (0, 0);
						switch (axis)
						{
							case MouseAxis.Horizontal: movement2D.x = value; break;
							case MouseAxis.Vertical: movement2D.y = value; break;
							case MouseAxis.Depth_Scroll:
							default:
								throw new NotImplementedException(axis.ToString());
						}
						HardwareInterface.INPUT_SIMULATOR.Mouse.MoveMouseBy((int)movement2D.x, (int)movement2D.y);
					}
					else // go to main thread and move it there
					{
						Program.CallOnceOverUIThread( delegate
						{
							float2 pos = HardwareInterface.MousePostion;
							int indexOfValue;
							switch (axis)
							{
								case MouseAxis.Horizontal:
									indexOfValue = 0;
									break;
								case MouseAxis.Vertical:
									indexOfValue = 1;
									break;
								case MouseAxis.Depth_Scroll:
								default:
									throw new NotImplementedException(axis.ToString());
							}

							RectangleF screenArea = Screen.PrimaryScreen.Bounds;
							float2 screensize = Float2.Absolute((screenArea.Width, screenArea.Height));
							pos[indexOfValue] = value * screensize[indexOfValue];
							HardwareInterface.MousePostion = pos;
						});
					}
				}

				void SimulateKeyReceived (DIKey hwKey, VirtualKeyCode vkc, ButtonEvent keyState)
				{
					if(currentInputTarget.SimulateInputAllowed(activeWindow) == false)
						return;

					switch (keyState)
					{
						case ButtonEvent.Pressed: HardwareInterface.INPUT_SIMULATOR.Keyboard.KeyDown(vkc); break;
						case ButtonEvent.Released: HardwareInterface.INPUT_SIMULATOR.Keyboard.KeyUp(vkc); break;
						default: throw new NotImplementedException($"{keyState}");
					}
				}

				void SimulateGamepad(Gamepad newState, int index)
				{
					SimGamePad.Instance.PlugIn(index);
					SimGamePad.Instance.State[ index ].FromSharpDX(newState);
					SimGamePad.Instance.Update( index );
				}

				void RemovedMappingUnplug(GamepadSource source, int index, int amountOnSlot)
				{
					if(amountOnSlot == 0)
						SimGamePad.Instance.Unplug(index);
				}



				#endregion




				uiApp = new Application();
				uiApp.UnhandledException += (sender, e) => UnhandledException(e.ExceptionObject);
				//uiApp.Terminating += (c, a) => { uiApp = null; };
				new Thread( () =>
				{
					while (exiting == false)
					{
						uiApp ? .Invoke( delegate { LowPriorityUILoop ? .Invoke(); } );
						Thread.Sleep( UI_LOOP_MS );
					}
				} ).Start();
				new Thread( () =>
				{
					while (exiting == false)
					{
						uiApp ? .Invoke( delegate { HighPriorityUILoop ? .Invoke(); } );
						Thread.Sleep( UI_LOOP_HIGHPRIORITY_MS );
					}
				} ).Start();



				uiApp.Run(MainWindow = new MainWindow ( () => Exit_Safe( ExitMotif.MainWindowClosed )));return;
				// Run() blocks everything untill we close ; nothing runs past here

			}
			catch (Exception e)
			{
				UnhandledException(e);
			}
		}







		public static void LogDeviceChangesToTerminal(bool on)
		{
			d.log(on);
			if (on)
			{
				HardwareInterface.KEYBOARD_STATE.OnKeyChange -= KeyboardKeyChange;
				HardwareInterface.KEYBOARD_STATE.OnKeyChange += KeyboardKeyChange;

				HardwareInterface.MOUSE_STATE.OnAxisChange -= MouseAxisChange;
				HardwareInterface.MOUSE_STATE.OnAxisChange += MouseAxisChange;
				HardwareInterface.MOUSE_STATE.OnButtonChange -= MouseButtonChange;
				HardwareInterface.MOUSE_STATE.OnButtonChange += MouseButtonChange;

				GamepadMappings.GamepadStateChanged -= GamepadStateChange;
				GamepadMappings.GamepadStateChanged += GamepadStateChange;
			}
			else
			{
				HardwareInterface.KEYBOARD_STATE.OnKeyChange -= KeyboardKeyChange;
				HardwareInterface.MOUSE_STATE.OnAxisChange   -= MouseAxisChange;
				HardwareInterface.MOUSE_STATE.OnButtonChange -= MouseButtonChange;
				GamepadMappings.GamepadStateChanged -= GamepadStateChange;
			}


			void KeyboardKeyChange(DIKey key, VirtualKeyCode code, ButtonEvent bEvent)
			{
				d.log($"{nameof(KeyboardKeyChange)}: {key} {code} {bEvent}");
			}
			void GamepadStateChange (Gamepad state, int index)
			{
				d.log($"{nameof(GamepadStateChange)}: {index} {state.Print()}");
			}
			void MouseAxisChange(float value, MouseAxis axis, bool relative)
			{
				d.log($"{nameof(MouseAxisChange)}: {value} {axis} {relative}");
			}
			void MouseButtonChange(int buttonIndex, ButtonEvent buttonEvent)
			{
				d.log($"{nameof(MouseButtonChange)}: {buttonIndex} {buttonEvent}");
			}
		}





		public static void CallOnceOverUIThread(Action a)
		{
			uiApp ? .AsyncInvoke(a);
		}


		public static void ForceUIUpdate()
		{
			uiApp.RunIteration();
		}








		public static bool exiting = false;
		public static void Exit_Safe(ExitMotif motif)
		{
			if(exiting)
				return;
			exiting = true;
			Task t = Task.Run( async() =>
			{
				while (logicLoop.IsAlive)
				{
					Thread.Sleep(16);
				}

				await NetworkProgramLayer.ShutdownOnApplicationQuit(motif);

				OnApplicationQuit ? .Invoke(motif);
			} );

			// Wait for a maximum of 2 seconds for the task to complete
			t.Wait( 2000 );

			try
			{
				// Check wheter we still have a bunch of threads running in the background, either because of the quit
				// task taking longer than 2 seconds or when threads dont terminate properly
				// Note that we always have at least a single thread running even when closing ; the thread running this code
				int count = 0;
				foreach (ProcessThread thread in Process.GetCurrentProcess().Threads)
				{
					if (thread.ThreadState == ThreadState.Running)
						count++;
				}
				if (count > 1)
				{
					d.logwarning($"{count -1} thread(s) still running, leaving them one more second before terminating");
					Thread.Sleep(1000);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
			Environment.Exit(0);
		}




		static void UnhandledException(object obj)
		{
			try
			{
				string outStr;
				if (obj is Exception ex)
					outStr = $"{ex.Message}\n{ex}";
				else
					outStr = obj.ToString();
				outStr = $"\nUnhandled exception occured, Make sure that your .NET versions are installed and up to date:\n{outStr}";
				d.logerror(outStr);
				// We cannot show any UI dialog without a window and it the window to be properly init before hand, so checking for visible as well.
				MessageBox_Error(outStr);
			}
			catch (Exception e)
			{
				d.logerror(e);
			}
		}




























		public static void MessageBox_Error(string msg)
		{
			MessageBox(msg, MessageBoxType.Error, MessageBoxButtons.OK, MessageBoxDefaultButton.OK);
		}

		public static bool MessageBox_YesNo(string msg)
		{
			return MessageBox(msg, MessageBoxType.Question, MessageBoxButtons.YesNo, MessageBoxDefaultButton.Yes) == DialogResult.Yes;
		}


		public static DialogResult MessageBox(string message, MessageBoxType type, MessageBoxButtons buttonsType, MessageBoxDefaultButton defaultButton)
		{
			return Eto.Forms.MessageBox.Show( message, buttonsType, type, defaultButton );
		}

	}





	public static class NetworkProgramLayer
	{
		public enum NetworkMode
		{
			Idle,
			Host,
			Client
		}
		public static NetworkMode networkState { get; private set; }

		public static bool IsHost => networkState == NetworkMode.Host;
		public static bool IsClient => networkState == NetworkMode.Client;
		public static bool IsIdle => networkState == NetworkMode.Idle;

		public static event System.Action OnHost, OnClient, OnIdle, OnSwitching;


		static NetworkProgramLayer()
		{
			Network.OnShuttingDown += (s) => OnSwitching ? .Invoke();
			Network.OnShutdown += (s) => OnIdle ? .Invoke();
			Network.OnPeerDisconnect += (peer, info) =>
			{
				if (IsHost == false)
				{
					Stop("Host disconnected");
				}
			};
		}


		public static async Task ShutdownOnApplicationQuit(ExitMotif m)
		{
			await Network.ShutdownNetwork($"Application Quitting {m}");
		}

		public static async void Host(ushort port)
		{
			OnSwitching ? .Invoke();

			Network n = await Network.StartNetwork(16, port, "DefaultPassword");

			if (n != null)
			{
				networkState = NetworkMode.Host;
				OnHost ? .Invoke();
			}
			else
			{
				networkState = NetworkMode.Idle;
				OnIdle ? .Invoke();
			}
		}


		public static async Task<ConnectionTask.Result> Join(string ip, ushort port)
		{
			OnSwitching ? .Invoke();

			Network n = await Network.StartNetwork(16, unchecked((ushort)(port+1)), "DefaultPassword");
			if (n == null)
			{
				OnIdle ? .Invoke();
				return ConnectionTask.Result.Failed;
			}

			ConnectionTask cTask = await Network.ConnectToNetwork(ip, port);
			switch (cTask.result)
			{
				case ConnectionTask.Result.Failed:
					networkState = NetworkMode.Idle;
					OnIdle ? .Invoke();
					break;
				case ConnectionTask.Result.Successful:
					networkState = NetworkMode.Client;
					OnClient ? .Invoke();
					break;
				case ConnectionTask.Result.Waiting:
				case ConnectionTask.Result.AlreadyConnected:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			return cTask.result;
		}


		public static async void Stop(string cause)
		{
			OnSwitching ? .Invoke();
			await Network.ShutdownNetwork(cause);
			networkState = NetworkMode.Idle;
			OnIdle ? .Invoke();
		}
	}


	public struct InputTarget
	{
		public static InputTarget None = new InputTarget(TargetKind.None);
		public static InputTarget Any = new InputTarget(TargetKind.Any);





		enum TargetKind
		{
			Any,
			SpecificProcess,
			None
		}





		readonly Process process;
		readonly TargetKind kind;




		public InputTarget(Process newProcess)
		{
			process = newProcess;
			kind = TargetKind.SpecificProcess;
		}


		InputTarget(TargetKind newKind)
		{
			kind = newKind;
			process = null;
		}




		public bool SimulateInputAllowed(Process focusedProcess)
		{
			switch (kind)
			{
				case TargetKind.None:
					return false;
				case TargetKind.Any:
					return true;
				case TargetKind.SpecificProcess:
					return focusedProcess != null && focusedProcess.Id == process.Id;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}


		public override string ToString()
		{
			switch (kind)
			{
				case TargetKind.None:
					return "NONE";
				case TargetKind.Any:
					return "ANY";
				case TargetKind.SpecificProcess:
					return process.ProcessName;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}


		public static bool operator ==(InputTarget a, InputTarget b)
		{
			if (a.kind != b.kind)
				return false;

			if (a.kind == TargetKind.SpecificProcess)
			{
				if (a.process == null && b.process == null)
					return true;
				else if (b.process == null)
					return false;
				return a.process.Id == b.process.Id;
			}
			return true;
		}


		public static bool operator !=(InputTarget a, InputTarget b)
		{
			return !(a == b);
		}


		public override bool Equals(object obj)
		{
			if (obj is InputTarget it)
				return it == this;
			return false;
		}


		public override int GetHashCode()
		{
			unchecked
			{
				d.logwarning($"{nameof(InputTarget)}'s {nameof(GetHashCode)} was generated, verify it's accuracy before use");
				return ((process != null ? process.GetHashCode() : 0) * 397) ^ (int) kind;
			}
		}
	}





	public static class ProcessHelper
	{
		[DllImport("user32.dll")]
		static extern IntPtr GetForegroundWindow();

		public static Process GetFocusedProcess(IList<Process> processes = null)
		{
			IntPtr activeWindowHandle = GetForegroundWindow();
			if (processes == null)
				processes = Process.GetProcesses();
			foreach(Process p in processes)
			{
				if(p.MainWindowHandle == activeWindowHandle)
					return p;
			}
			return null;
		}

		static readonly HashSet<int> processIdFilteredOut = new HashSet<int>();
		static readonly HashSet<int> processIdFilteredIn = new HashSet<int>();

		static readonly HashSet<string> staticProcessFilter = new HashSet<string>()
		{
			"svchost",
			"winlogon",
			"RuntimeBroker",
			"MSBuild",
			"ctfmon",
			"csrss",
			"SearchIndexer",
			"conhost",
			"dwm",
			"SearchFilterHost",
			"WUDFHost",
			"SearchProtocolHost",
			"NLSvc",
			"fontdrvhost",
			"Idle",
			"System",
			"dasHost",
		};

		public static async Task FilterProcess(Process[] input, List<Process> output, bool diffusePerformance)
		{
			output.Clear();
			Stopwatch sw = Stopwatch.StartNew();

			foreach(Process p in input)
			{
				try
				{
					if (processIdFilteredIn.Contains(p.Id))
					{
						output.Add(p);
						continue;
					}
					if (staticProcessFilter.Contains(p.ProcessName) || p.ProcessName.Contains("service"))
						continue;

					// Fetching the window handle takes a significant amount of time, as such I'm introducing a delay
					// between every call to avoid wasting performances since this is not a time-critical function
					// We can't filter process out based on the first detection of window handles either since the process
					// might not start with one but get one a couple of seconds after it started
					IntPtr windowHandle = p.MainWindowHandle;
					if(diffusePerformance)
						await Task.Delay(1);


					if (windowHandle == IntPtr.Zero)
						continue;
					processIdFilteredIn.Add(p.Id);
					output.Add(p);
					continue;
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					//throw;
				}
			}
		}

	}










	public enum ExitMotif
	{
		MainWindowClosed,
		GamepadSimInitException
		// Add yours here
	}
}
