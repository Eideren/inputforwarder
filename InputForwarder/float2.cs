﻿// ReSharper disable InconsistentNaming
// ReSharper disable MemberCanBePrivate.Global



// Comment out to disable eto forms PointF support
#define ETOPOINTF_SUPPORT
// Comment out to disable inlining
#define FORCE_INLINING

using SysMath = System.Math;
using InvalidOperationException = System.InvalidOperationException;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using ObsoleteAttribute = System.ObsoleteAttribute;

#if FORCE_INLINING
using Implementation = System.Runtime.CompilerServices.MethodImplAttribute;
using Options = System.Runtime.CompilerServices.MethodImplOptions;
#endif

#if ETOPOINTF_SUPPORT
using Eto.Drawing;
#endif








namespace EMath
{
	public struct float2
	{
		public float x, y;

		/// <summary>
		/// Gets this vector's value over a new vector with swapped axis
		/// </summary>
		public float2 yx => new float2(y, x);

		/// <summary>
		/// Gets this vector's x value over a new vector on both axis
		/// </summary>
		public float2 xx => new float2(x, x);

		/// <summary>
		/// Gets this vector's y value over a new vector on both axis
		/// </summary>
		public float2 yy => new float2(y, y);

		public float this[int v]
		{
			get
			{
				if (v == 0)
					return x;
				else if (v == 1)
					return y;
				else
					throw new ArgumentOutOfRangeException(v.ToString());
			}
			set
			{
				if (v == 0)
					x = value;
				else if (v == 1)
					y = value;
				else
					throw new ArgumentOutOfRangeException(v.ToString());
				return;
			}
		}

		#region CONSTRUCTOR

		public float2(float value)
		{
			x = value;
			y = value;
		}

		public float2(float newX, float newY)
		{
			x = newX;
			y = newY;
		}

		public float2((float x, float y)tuple)
		{
			x = tuple.x;
			y = tuple.y;
		}

		#endregion




		#region OPERATOR

		[Implementation(Inline)]
		public static float2 operator +(float2 a, float2 b)
		{
			return new float2(a.x + b.x, a.y + b.y);
		}

		[Implementation(Inline)]
		public static float2 operator -(float2 a, float2 b)
		{
			return new float2(a.x - b.x, a.y - b.y);
		}
		[Implementation(Inline)]
		public static float2 operator -(float2 a)
		{
			return new float2(-a.x, -a.y);
		}

		[Implementation(Inline)]
		public static float2 operator *(float2 a, float2 b)
		{
			return new float2(a.x * b.x, a.y * b.y);
		}
		[Implementation(Inline)]
		public static float2 operator *(float2 a, float m)
		{
			return new float2(a.x * m, a.y * m);
		}

		[Implementation(Inline)]
		public static float2 operator *(float m, float2 a)
		{
			return new float2(a.x * m, a.y * m);
		}

		[Implementation(Inline)]
		public static float2 operator /(float2 a, float2 b)
		{
			return new float2(a.x / b.x, a.y / b.y);
		}
		[Implementation(Inline)]
		public static float2 operator /(float2 a, float d)
		{
			return new float2(a.x / d, a.y / d);
		}

		#endregion



		#region EQUALITY

		[Implementation(Inline)]
		public static bool operator ==(float2 a, float2 b)
		{
			// We expect exact equality when using this operator, do not round it.
			return a.x == b.x && a.y == b.y;
		}

		[Implementation(Inline)]
		public static bool operator !=(float2 a, float2 b)
		{
			// We expect exact inequality when using this operator, do not round it.
			return a.x != b.x || a.y != b.y;
		}

		public override bool Equals(object other)
		{
			if (other is float2 float2)
				return this == float2;
			else
				return false;
		}

		#endregion



		#region CONVERSION

		[Implementation(Inline)]
		public static implicit operator (float x, float y)( float2 f2 )
		{
			return (f2.x, f2.y);
		}
		[Implementation(Inline)]
		public static implicit operator float2( (float x, float y) t2 )
		{
			return new float2(t2.x, t2.y);
		}
		[Implementation(Inline)]
		public static explicit operator float2( float value )
		{
			return new float2(value, value);
		}

		#if ETOPOINTF_SUPPORT
		[Implementation(Inline)]
		public static implicit operator PointF( float2 f2 )
		{
			return new PointF(f2.x, f2.y);
		}
		[Implementation(Inline)]
		public static implicit operator float2( PointF pf )
		{
			return new float2(pf.X, pf.Y);
		}
		#endif

		#endregion












		public override string ToString() => $"({x}, {y})";

		public string ToString(string format) => $"({x.ToString(format)}, {y.ToString(format)})";

		/// <summary>
		/// This struct's x and y values aren't readonly, if they change our hash will as well and hashtables expect
		/// constant hash for objects. So convert/cast your float2 to float2f and use that instead.
		/// </summary>
		[ObsoleteAttribute("Convert/cast your float2 to float2f instead, this type is not hashtable safe")]
		public override int GetHashCode() => throw new InvalidOperationException($"Use/convert to {nameof(float2f)} instead, this type is not hashtable safe");

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}





	/// <summary>
	/// Readonly version of float2, ONLY used to fetch safe HashCodes. To create one you can cast your float2 to float2f.
	/// </summary>
	public struct float2f
	{
		readonly float x, y;

		float2f(float newX, float newY)
		{
			x = newX;
			y = newY;
		}

		[Implementation(Inline)]
		public static implicit operator float2( float2f f2f )
		{
			return new float2(f2f.x, f2f.y);
		}
		[Implementation(Inline)]
		public static implicit operator float2f( float2 f2 )
		{
			return new float2f(f2.x, f2.y);
		}

		public override int GetHashCode() => x.GetHashCode() ^ (y.GetHashCode() << 2);

		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;
	}



	public static class Float2
	{
		/// <summary>
		/// Shortcut for code readability
		/// </summary>
		const Options Inline = Options.AggressiveInlining;

		/// <summary>
		/// Returns the length of a vector ; the distance between it and (0, 0)
		/// </summary>
		[Implementation(Inline)]
		public static float Length(this float2 f2)
		{
			return (float)SysMath.Sqrt(SquaredLength(f2));
		}

		/// <summary>
		/// Returns the squared length of a vector, faster than Length() but the returned value is not the length in
		/// unit and as such is mostly used for fast comparison between multiple vectors.
		/// </summary>
		[Implementation(Inline)]
		public static float SquaredLength(this float2 f2)
		{
			return (f2.x * f2.x + f2.y * f2.y);
		}

		/// <summary>
		/// Returns the dot product between two vectors. Often used with normalized vectors to find out wheter they
		/// point to the same direction.
		/// </summary>
		[Implementation(Inline)]
		public static float Dot(this float2 a, float2 b)
		{
			return a.x * b.x + a.y * b.y;
		}

		/// <summary>
		/// Returns a vector going in the same direction with a length of one.
		/// </summary>
		[Implementation(Inline)]
		public static float2 Normalized(this float2 f2)
		{
			float length = SquaredLength(f2);
			return length > 0f ? f2 / (float)SysMath.Sqrt(length) : new float2(0f);
		}

		/// <summary>
		/// Returns the real distance in unit between those two points.
		/// </summary>
		[Implementation(Inline)]
		public static float Distance(this float2 a, float2 b)
		{
			return Length(a - b);
		}

		/// <summary>
		/// Returns a vector where each values of the source vector are between min and max.
		/// </summary>
		[Implementation(Inline)]
		public static float2 Clamped(this float2 src, float min, float max)
		{
			return new float2(src.x > max ? max : src.x < min ? min : src.x,
			                  src.y > max ? max : src.y < min ? min : src.y);
		}

		/// <summary>
		/// Returns a vector whose values are made absolute ; will swap to the positive value when it's negative.
		/// </summary>
		[Implementation(Inline)]
		public static float2 Absolute(this float2 src)
		{
			return new float2(src.x < 0f ? -src.x : src.x,
			                  src.y < 0f ? -src.y : src.y);
		}

		/// <summary>
		/// Returns a point on a line between 'zero' and 'one' based on 't''s value from 0 to 1. If 't' isn't between those
		/// values, the point will lay further along that line, outside of the segment zero->one.
		/// </summary>
		[Implementation(Inline)]
		public static float2 Lerp(float2 zero, float2 one, float t)
		{
			return new float2(zero.x + (one.x - zero.x) * t, zero.y + (one.y - zero.y) * t);
		}
	}




	#if !FORCE_INLINING
	/// <summary>
	/// Dummy class to circumvent inlining when required
	/// </summary>
	public class ImplementationAttribute : Attribute
	{
		public ImplementationAttribute(Options mio){}
	}

	public enum Options
	{
		AggressiveInlining
	}
	#endif
}