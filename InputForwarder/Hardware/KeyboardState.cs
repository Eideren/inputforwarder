﻿using System;
using System.Collections;
using ENet;
using DIKey = SharpDX.DirectInput.Key;
using VKC =	WindowsInput.Native.VirtualKeyCode;






namespace InputForwarder.Hardware
{

	public class KeyboardState
	{
		public event Action<DIKey, VKC, ButtonEvent> OnKeyChange;
		BitArray mask = new BitArray(HardwareInterface.FILTERED_KEYS_COUNT());




		public KeyboardState()
		{
			Program.OnApplicationQuit += (e) => Clear(true);
			// I have no clue why but apparently creating the BitArray on class init/inside the constructor doesn't actually set the array to the specified length
			mask.Length = HardwareInterface. FILTERED_KEYS_COUNT();
		}



		public void TryFeed(DIKey key, ButtonEvent buttonEvent)
		{
			if(HardwareInterface.ValidKey(key))
				Feed(HardwareInterface.GetIndex(key), buttonEvent);
			else
				d.logwarning($"{key} is filtered out");
		}
		public void Feed(byte key, ButtonEvent buttonEvent)
		{
			Feed(key, buttonEvent == ButtonEvent.Pressed);
		}
		 void Feed(byte key, bool keydown)
		{
			// Filter out unmodified key
			if (mask[key] == keydown)
				return;

			mask[key] = keydown;
			(DIKey dxKey, VKC vkcKey) = HardwareInterface.GetKeyFromIndex(key);
			OnKeyChange ? .Invoke( dxKey, vkcKey, keydown ? ButtonEvent.Pressed : ButtonEvent.Released );
		}


		public void Feed(byte[] buffer, ref int head)
		{
			int compRollingIndex = 0;
			byte received8Bits = buffer[head++];
			// Go through our buffer per bit instead of per byte to avoid allocating new BitArrays; every eight iterations, go to the next byte
			for (byte key = 0; key < HardwareInterface. FILTERED_KEYS_COUNT(); key++)
			{
				if (compRollingIndex >= BYTE_COMP.Length)
				{
					compRollingIndex = 0;
					received8Bits = buffer[head++];
				}

				// Is key down
				bool keyPushedDown = (BYTE_COMP[compRollingIndex] & received8Bits) == BYTE_COMP[compRollingIndex];
				Feed(key, keyPushedDown);

				compRollingIndex++;
			}
		}


		public void Clear(bool allowRaisingEvents)
		{
			if (allowRaisingEvents)
			{
				for (byte key = 0; key < HardwareInterface. FILTERED_KEYS_COUNT(); key++)
				{
					Feed(key, false);
				}
			}
			else
				mask = new BitArray(HardwareInterface. FILTERED_KEYS_COUNT());
		}

		public byte[] ToByteArray()
		{
			byte[] ret = new byte[(mask.Length - 1) / 8 + 1];
			mask.CopyTo(ret, 0);
			return ret;
		}


		public void Serialize(ByteBuffer buffer)
		{
			int compRollingIndex = 0;
			byte byteUnderConstruction = 0;
			for (byte key = 0; key < HardwareInterface.FILTERED_KEYS_COUNT(); key++)
			{
				if (mask[key])
				{
					byteUnderConstruction |= BYTE_COMP[compRollingIndex];
				}

				compRollingIndex++;
				// Done all 8 bits, reset
				if (compRollingIndex >= BYTE_COMP.Length)
				{
					buffer.Add(byteUnderConstruction);
					compRollingIndex = 0;
					byteUnderConstruction = 0;
				}
			}
			if(compRollingIndex > 0)
				buffer.Add(byteUnderConstruction);
		}


		static readonly byte[] BYTE_COMP =
		{
			0b0000_0001,
			0b0000_0010,
			0b0000_0100,
			0b0000_1000,
			0b0001_0000,
			0b0010_0000,
			0b0100_0000,
			0b1000_0000,
		};
	}
}