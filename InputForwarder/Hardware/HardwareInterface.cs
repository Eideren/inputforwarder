﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using WindowsInput;
using WindowsInput.Native;

using EMath;

using SimWinInput;

// XInput
using Controller = SharpDX.XInput.Controller;
using UserIndex = SharpDX.XInput.UserIndex;
// Direct Input
using DirectInputInstance = SharpDX.DirectInput.DirectInput;
using DIKeyboard = SharpDX.DirectInput.Keyboard;
using DIKey = SharpDX.DirectInput.Key;
using VKC =	WindowsInput.Native.VirtualKeyCode;
using DIMouse = SharpDX.DirectInput.Mouse;



namespace InputForwarder.Hardware
{
	public class LocalGamepadSource : GamepadSource
	{
		public bool IsConnected;
		
		public LocalGamepadSource( int sourceSlot ) : base( sourceSlot )
		{
		}



		public override string ToString()
		{
			return $"Local gamepad #{SourceSlot} {(IsConnected?"":"Disconnected")}";
		}
	}



	public static class HardwareInterface
	{
		public static readonly InputSimulator INPUT_SIMULATOR = new InputSimulator();
		// Do not init it here, keep it inside this statics's ctor, if you do it here it will try to access FILTERED_KEYS without running its ctor first, not sure why c# fails here
		public static readonly KeyboardState KEYBOARD_STATE;
		public static readonly MouseState MOUSE_STATE;
		public static readonly LocalGamepadSource[] GAMEPADS;

		static bool _pollingKeyboard = false, _pollingMouse = false;

		public static float2 MousePostion
		{
			get => Eto.Forms.Mouse.Position;
			set => Eto.Forms.Mouse.Position = value;
		}






		static HardwareInterface()
		{
			init = true;
			
			const int GAMEPAD_COUNT = 4;
			GAMEPADS = new LocalGamepadSource[GAMEPAD_COUNT];
			for (int i = 0; i < GAMEPAD_COUNT; i++)
				GAMEPADS[i] = new LocalGamepadSource(i);
			
			KEY_TO_FILTERED_MAPPING = new Dictionary<DIKey, byte>(FILTERED_KEYS_COUNT());

			int keyIndex = 0;
			FILTERED_KEYS_INDEXED = new (DIKey, VirtualKeyCode)[FILTERED_KEYS_COUNT()];
			foreach (KeyValuePair<DIKey,VirtualKeyCode> kvp in FILTERED_KEYS)
			{
				FILTERED_KEYS_INDEXED[keyIndex] = (kvp.Key, kvp.Value);
				KEY_TO_FILTERED_MAPPING.Add( kvp.Key, checked((byte)keyIndex));
				keyIndex++;
			}
			KEYBOARD_STATE = new KeyboardState();
			MOUSE_STATE = new MouseState();


			// MANUALLY INSTALL DRIVER, INSTALLING IT THROUGH THE LIB FAILS TO DELIVER A PROPER USER MESSAGE WHEN IT FAILS
			while (true)
			{
				try
				{
					ScpBus bus = new ScpBus();
					break;
				}
				catch (IOException ex)
				{
					d.logerror(ex);
					const string DRIVER_EXE = "ScpDriverInstaller.exe";
					const string CMDS = "/install";
					string driverSupposedDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
					string driverMessage = $"We need to install a driver to simulate gamepads while hosting, choosing 'No' will break support for gamepads.\nYou can safely uninstall it by running '{DRIVER_EXE}' located under {driverSupposedDir}";

					if (Program.MessageBox_YesNo(driverMessage) == false)
						return;

					string driverLocation = Path.Combine(driverSupposedDir, DRIVER_EXE);

					if (File.Exists(driverLocation))
						System.Diagnostics.Process.Start(driverLocation, CMDS).WaitForExit();
					else
						Program.MessageBox_Error($"Couldn't find driver executable '{DRIVER_EXE}', verify that it exists in {driverSupposedDir}");
				}
			}
			SimGamePad.Instance.Initialize(false);
			Program.OnApplicationQuit += motif => SimGamePad.Instance.ShutDown();


			BUTTON_TO_FILTERED_MAPPING = new Dictionary<GamePadControl, byte>(FILTERED_BUTTONS_COUNT());
			for (int i = 0; i < FILTERED_BUTTONS_COUNT(); i++)
			{
				BUTTON_TO_FILTERED_MAPPING.Add(FILTERED_BUTTONS[i], checked((byte)i));
			}
			
			Thread pollingThread = new Thread(PollHardwareLoop);
			pollingThread.Start();
		}


		static void PollHardwareLoop()
		{
			Controller[] controllers =
			{
				new Controller(UserIndex.One),
				new Controller(UserIndex.Two),
				new Controller(UserIndex.Three),
				new Controller(UserIndex.Four),
			};

			using (DirectInputInstance dii = new DirectInputInstance())
			using (DIKeyboard keyboard = new DIKeyboard(dii))
			using (DIMouse mouse = new DIMouse(dii))
			{
				mouse.Properties.BufferSize = 128;
				keyboard.Properties.BufferSize = 128;
				keyboard.Acquire();
				mouse.Acquire();

				while (Program.exiting == false)
				{
					if(Settings.HardwarePollingRate != 0)
						Thread.Sleep((int)(1000d / Settings.HardwarePollingRate));
					
					// Controllers
					for (int i = 0; i < controllers.Length; i++)
					{
						var sharpsidePad = controllers[i];
						var gamepadSource = GAMEPADS[i];
						gamepadSource.IsConnected = sharpsidePad.IsConnected;
						if(sharpsidePad.IsConnected == false)
							continue;
						
						if( sharpsidePad.GetState( out var state ) )
							gamepadSource.State = state.Gamepad;
						else
							d.log("Couldn't get state");
					}

					keyboard.Poll();
					if (_pollingKeyboard)
					{
						foreach (var stateChanges in keyboard.GetBufferedData())
						{
							KEYBOARD_STATE.TryFeed( stateChanges.Key, stateChanges.IsPressed ? ButtonEvent.Pressed : ButtonEvent.Released );
						}
					}

					mouse.Poll();
					if (_pollingMouse)
					{
						foreach (var stateChanges in mouse.GetBufferedData())
						{
							MOUSE_STATE.Feed(stateChanges);
						}
					}
				}
				keyboard.Unacquire();
				mouse.Unacquire();
			}
		}


		public static void ToggleKeyboardPolling(bool pollKeyboard)
		{
			if(_pollingKeyboard == pollKeyboard)
				return;
			_pollingKeyboard = pollKeyboard;
			KEYBOARD_STATE.Clear(true);
		}
		public static void ToggleMousePolling(bool pollMouse)
		{
			if(_pollingMouse == pollMouse)
				return;
			_pollingMouse = pollMouse;
			MOUSE_STATE.Clear(true);
		}



		#region KEYBOARD
		public static (DIKey, VKC) GetKeyFromIndex(byte index)
		{
			return FILTERED_KEYS_INDEXED[index];
		}
		public static byte GetIndex(DIKey hwKey)
		{
			if (KEY_TO_FILTERED_MAPPING.TryGetValue(hwKey, out byte value))
				return value;
			throw new ArgumentException($"Key {hwKey} is not contained into the filtered keys", nameof(hwKey));
		}
		/// <summary>
		/// Reverse dictionary lookup => slow
		/// </summary>
		public static byte GetIndex(VKC hwKey)
		{
			if (FILTERED_KEYS.ContainsValue(hwKey))
			{
				if (KEY_TO_FILTERED_MAPPING.TryGetValue(FILTERED_KEYS.First( kvp => kvp.Value == hwKey ).Key, out byte value))
					return value;
			}
			throw new ArgumentException($"Key {hwKey} is not contained into the filtered keys", nameof(hwKey));
		}
		public static bool ValidKey(DIKey key)
		{
			return FILTERED_KEYS.ContainsKey(key);
		}
		public static bool ValidKey(VKC key)
		{
			return FILTERED_KEYS.ContainsValue(key);
		}

		public static VKC DXKeyToVKC(DIKey hwKey)
		{
			return FILTERED_KEYS[hwKey];
		}

		/// <summary>
		/// Reverse dictionary lookup => slow
		/// </summary>
		public static DIKey VKCToDXKey(VKC hwKey)
		{
			if (FILTERED_KEYS.ContainsValue(hwKey))
			{
				return FILTERED_KEYS.First( kvp => kvp.Value == hwKey ).Key;
			}
			throw new ArgumentException($"Key {hwKey} is not contained into the filtered keys", nameof(hwKey));
		}
		#endregion


		#region GAMEPAD
		public static GamePadControl GetGampadButton(byte index)
		{
			return FILTERED_BUTTONS[index];
		}
		public static byte GetIndex(GamePadControl button)
		{
			if (BUTTON_TO_FILTERED_MAPPING.TryGetValue(button, out byte value))
				return value;
			throw new ArgumentException($"Button {button} is not contained into the filtered keys", nameof(button));
		}
		public static bool ValidButon(GamePadControl button)
		{
			return BUTTON_TO_FILTERED_MAPPING.ContainsKey(button);
		}

		/*public static VKC HardwareKeyToVKC(HardwareKey hwKey)
		{
			return (VKC) hwKey;
		}*/
		#endregion


		#region FILTER

		static readonly bool init;
		/// <summary>
		/// Contains the keys that we will simulate.
		/// We need to fit the largest amount of keys in the smallest amount of bytes to send entire keyboard state per message
		/// to be able to mostly ignore packet loss and in terms use the fastest possible send method and to reduce our network throughput
		/// </summary>
		static readonly Dictionary<DIKey, VKC> FILTERED_KEYS = new Dictionary<DIKey, VKC>()
		{
			{ DIKey.Escape, VKC.ESCAPE },
			{ DIKey.D1, VKC.VK_1 },
			{ DIKey.D2, VKC.VK_2 },
			{ DIKey.D3, VKC.VK_3 },
			{ DIKey.D4, VKC.VK_4 },
			{ DIKey.D5, VKC.VK_5 },
			{ DIKey.D6, VKC.VK_6 },
			{ DIKey.D7, VKC.VK_7 },
			{ DIKey.D8, VKC.VK_8 },
			{ DIKey.D9, VKC.VK_9 },
			{ DIKey.D0, VKC.VK_0 },
			{ DIKey.Minus, VKC.OEM_MINUS },
			{ DIKey.Equals, VKC.OEM_PLUS },
			{ DIKey.Back, VKC.BACK },
			{ DIKey.Tab, VKC.TAB },
			{ DIKey.Q, VKC.VK_Q },
			{ DIKey.W, VKC.VK_W },
			{ DIKey.E, VKC.VK_E },
			{ DIKey.R, VKC.VK_R },
			{ DIKey.T, VKC.VK_T },
			{ DIKey.Y, VKC.VK_Y },
			{ DIKey.U, VKC.VK_U },
			{ DIKey.I, VKC.VK_I },
			{ DIKey.O, VKC.VK_O },
			{ DIKey.P, VKC.VK_P },
			{ DIKey.LeftBracket, VKC.OEM_4 },
			{ DIKey.RightBracket, VKC.OEM_6 },
			{ DIKey.Return, VKC.RETURN },
			{ DIKey.LeftControl, VKC.LCONTROL },
			{ DIKey.A, VKC.VK_A },
			{ DIKey.S, VKC.VK_S },
			{ DIKey.D, VKC.VK_D },
			{ DIKey.F, VKC.VK_F },
			{ DIKey.G, VKC.VK_G },
			{ DIKey.H, VKC.VK_H },
			{ DIKey.J, VKC.VK_J },
			{ DIKey.K, VKC.VK_K },
			{ DIKey.L, VKC.VK_L },
			{ DIKey.Semicolon, VKC.OEM_1 },
			{ DIKey.Apostrophe, VKC.OEM_7 },
			{ DIKey.Grave, VKC.OEM_3 },
			{ DIKey.LeftShift, VKC.LSHIFT },
			{ DIKey.Backslash, VKC.OEM_5 },
			{ DIKey.Z, VKC.VK_Z },
			{ DIKey.X, VKC.VK_X },
			{ DIKey.C, VKC.VK_C },
			{ DIKey.V, VKC.VK_V },
			{ DIKey.B, VKC.VK_B },
			{ DIKey.N, VKC.VK_N },
			{ DIKey.M, VKC.VK_M },
			{ DIKey.Comma, VKC.OEM_COMMA },
			{ DIKey.Period, VKC.OEM_PERIOD },
			{ DIKey.Slash, VKC.OEM_2 },
			{ DIKey.RightShift, VKC.RSHIFT },
			{ DIKey.Multiply, VKC.MULTIPLY },
			{ DIKey.LeftAlt, VKC.LMENU },
			{ DIKey.Space, VKC.SPACE },
			{ DIKey.F1, VKC.F1 },
			{ DIKey.F2, VKC.F2 },
			{ DIKey.F3, VKC.F3 },
			{ DIKey.F4, VKC.F4 },
			{ DIKey.F5, VKC.F5 },
			{ DIKey.F6, VKC.F6 },
			{ DIKey.F7, VKC.F7 },
			{ DIKey.F8, VKC.F8 },
			{ DIKey.F9, VKC.F9 },
			{ DIKey.F10, VKC.F10 },
			{ DIKey.NumberPad7, VKC.NUMPAD7 },
			{ DIKey.NumberPad8, VKC.NUMPAD8 },
			{ DIKey.NumberPad9, VKC.NUMPAD9 },
			{ DIKey.Subtract, VKC.SUBTRACT },
			{ DIKey.NumberPad4, VKC.NUMPAD4 },
			{ DIKey.NumberPad5, VKC.NUMPAD5 },
			{ DIKey.NumberPad6, VKC.NUMPAD6 },
			{ DIKey.Add, VKC.ADD },
			{ DIKey.NumberPad1, VKC.NUMPAD1 },
			{ DIKey.NumberPad2, VKC.NUMPAD2 },
			{ DIKey.NumberPad3, VKC.NUMPAD3 },
			{ DIKey.NumberPad0, VKC.NUMPAD0 },
			{ DIKey.Decimal, VKC.DECIMAL },
			{ DIKey.Oem102, VKC.OEM_102 },
			{ DIKey.F11, VKC.F11 },
			{ DIKey.F12, VKC.F12 },
			{ DIKey.Kana, VKC.KANA },
			{ DIKey.Convert, VKC.CONVERT },
			{ DIKey.NoConvert, VKC.NONCONVERT },
			{ DIKey.Kanji, VKC.KANJI },
			{ DIKey.NumberPadEnter, VKC.RETURN },
			{ DIKey.RightControl, VKC.RCONTROL },
			{ DIKey.NumberPadComma, VKC.OEM_PERIOD },
			{ DIKey.Divide, VKC.DIVIDE },
			{ DIKey.PrintScreen, VKC.SNAPSHOT },
			{ DIKey.RightAlt, VKC.RMENU },
			{ DIKey.Home, VKC.HOME },
			{ DIKey.Up, VKC.UP },
			{ DIKey.PageUp, VKC.PRIOR },
			{ DIKey.Left, VKC.LEFT },
			{ DIKey.Right, VKC.RIGHT },
			{ DIKey.End, VKC.END },
			{ DIKey.Down, VKC.DOWN },
			{ DIKey.PageDown, VKC.NEXT },
			{ DIKey.Insert, VKC.INSERT },
			//{ DXKey.Delete, VKC.DELETE },
			{ DIKey.LeftWindowsKey, VKC.LWIN },
			{ DIKey.RightWindowsKey, VKC.RWIN },
			{ DIKey.Applications, VKC.APPS },
		};
		static readonly (DIKey, VKC)[] FILTERED_KEYS_INDEXED;
		static readonly Dictionary<DIKey, byte> KEY_TO_FILTERED_MAPPING;
		public static int FILTERED_KEYS_COUNT()
		{
			if(init == false || FILTERED_KEYS.Count == 0)
				throw new TypeInitializationException("Method called while its class was not entirely init", null);
			return FILTERED_KEYS.Count;
		}









		static readonly GamePadControl[] FILTERED_BUTTONS =
		{
			//GamePadControl.None,
			GamePadControl.DPadUp,
			GamePadControl.DPadDown,
			GamePadControl.DPadLeft,
			GamePadControl.DPadRight,
			GamePadControl.Start,
			GamePadControl.Back,
			GamePadControl.LeftStickClick,
			GamePadControl.RightStickClick,
			GamePadControl.LeftShoulder,
			GamePadControl.RightShoulder,
			GamePadControl.Guide,
			GamePadControl.A,
			GamePadControl.B,
			GamePadControl.X,
			GamePadControl.Y,
			/*GamePadControl.LeftTrigger,
			GamePadControl.RightTrigger,
			GamePadControl.LeftStickLeft,
			GamePadControl.LeftStickRight,
			GamePadControl.LeftStickUp,
			GamePadControl.LeftStickDown,
			GamePadControl.RightStickLeft,
			GamePadControl.RightStickRight,
			GamePadControl.RightStickUp,
			GamePadControl.RightStickDown,
			GamePadControl.LeftStickAsAnalog,
			GamePadControl.RightStickAsAnalog,
			GamePadControl.DPadAsAnalog,*/
		};
		public static int FILTERED_BUTTONS_COUNT()
		{
			if(init == false || FILTERED_BUTTONS.Length == 0)
				throw new TypeInitializationException("Method called while its class was not entirely init", null);
			return FILTERED_BUTTONS.Length;
		}
		static readonly Dictionary<GamePadControl, byte> BUTTON_TO_FILTERED_MAPPING;

		#endregion
	}

	public enum ButtonEvent
	{
		Pressed,
		Released
	}
}