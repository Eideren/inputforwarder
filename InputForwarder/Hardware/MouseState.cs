﻿using System;
using ENet;
using SharpDX.DirectInput;
using DIMouseUpdate = SharpDX.DirectInput.MouseUpdate;








namespace InputForwarder.Hardware
{
	public class MouseState
	{
		public delegate void OnAxisChangeDelegate(float value, MouseAxis axis, bool relative);
		public delegate void OnButtonChangeDelegate(int buttonIndex, ButtonEvent buttonEvent);
		public event OnButtonChangeDelegate OnButtonChange;
		public event OnAxisChangeDelegate OnAxisChange;

		// Are we offseting the mouse or setting its position
		bool relativeCoordinates;
		(float x, float y, float z) coordinates;
		byte buttonsState;

		static readonly byte[] BYTE_COMP =
		{
			0b0000_0001,
			0b0000_0010,
			0b0000_0100,
			0b0000_1000,
			0b0001_0000,
			0b0010_0000,
			0b0100_0000,
			0b1000_0000,
		};

		public void Feed(DIMouseUpdate mouseUpdate)
		{
			if(mouseUpdate.IsButton == false && mouseUpdate.Value == 0)
				return;


			bool buttonPressed = mouseUpdate.Value != 0;
			switch (mouseUpdate.Offset)
			{
				case MouseOffset.X:
					FeedAxis(mouseUpdate.Value, MouseAxis.Horizontal, true);
					break;
				case MouseOffset.Y:
					FeedAxis(mouseUpdate.Value, MouseAxis.Vertical, true);
					break;
				case MouseOffset.Z:
					FeedAxis(mouseUpdate.Value, MouseAxis.Depth_Scroll, relativeCoordinates);
					return;
				case MouseOffset.Buttons0:
					FeedButton(0, buttonPressed);
					return;
				case MouseOffset.Buttons1:
					FeedButton(1, buttonPressed);
					return;
				case MouseOffset.Buttons2:
					FeedButton(2, buttonPressed);
					return;
				case MouseOffset.Buttons3:
					FeedButton(3, buttonPressed);
					return;
				case MouseOffset.Buttons4:
					FeedButton(4, buttonPressed);
					return;
				case MouseOffset.Buttons5:
					FeedButton(5, buttonPressed);
					return;
				case MouseOffset.Buttons6:
					FeedButton(6, buttonPressed);
					return;
				case MouseOffset.Buttons7:
					FeedButton(7, buttonPressed);
					return;
				default:
					throw new ArgumentOutOfRangeException($"{mouseUpdate.Offset}");
			}
		}
		public void FeedButton(int index, bool pressed)
		{
			if(index > 7)
				throw new ArgumentOutOfRangeException($"{index}");

			byte previousValue = buttonsState;

			if (pressed)
				buttonsState |= BYTE_COMP[index];
			else
				buttonsState &= (byte)~BYTE_COMP[index];

			if (previousValue != buttonsState)
				OnButtonChange ? .Invoke(index, pressed ? ButtonEvent.Pressed : ButtonEvent.Released);
		}


		public void FeedAxis(float value, MouseAxis axis, bool relative)
		{
			bool changed = false;
			if (relativeCoordinates != relative)
			{
				relativeCoordinates = relative;
				coordinates = (0, 0, 0);
				changed = true;
			}

			if (axis == MouseAxis.Horizontal)
			{
				if (relative || coordinates.x != value)
				{
					coordinates.x = value;
					changed = true;
				}
			}
			else if (axis == MouseAxis.Vertical)
			{
				if (relative || coordinates.y != value)
				{
					coordinates.y = value;
					changed       = true;
				}
			}
			else if (axis == MouseAxis.Depth_Scroll)
			{
				coordinates.z = value;
				changed = true;
			}
			else
				throw new NotImplementedException(axis.ToString());

			if(changed)
				OnAxisChange ? .Invoke( value, axis, relative );

		}



		public void Clear(bool allowRaisingEvents)
		{
			if (allowRaisingEvents)
			{
				for (int button = 0; button < 8; button++)
					FeedButton(button, false);
			}
			else
				buttonsState = 0;
		}

		public void SerializeButtons(ByteBuffer buffer)
		{
			buffer.Add(buttonsState);
		}


		public void FeedButtons(byte[] buffer, ref int head)
		{
			byte newButtonState = buffer[head++];
			byte oldButtonState = buttonsState;
			if(oldButtonState == newButtonState)
				return;
			for (int i = 0; i < 8; i++)
			{
				byte comp = BYTE_COMP[i];
				FeedButton(i, (newButtonState & comp) == comp);
			}
		}


		public void SerializeAxis(ByteBuffer buffer)
		{
			BinarySerializer.ObjectToBytes(relativeCoordinates, buffer);
			BinarySerializer.ObjectToBytes(coordinates.x,       buffer);
			BinarySerializer.ObjectToBytes(coordinates.y,       buffer);
			BinarySerializer.ObjectToBytes(coordinates.z,       buffer);
		}


		public void FeedAxis(byte[] buffer, ref int head)
		{
			bool relCoord = BinarySerializer.BytesToObject<bool>(buffer, ref head);
			(float x, float y, float z) newCoordinates = (BinarySerializer.BytesToObject<float>(buffer,    ref head),
				                                             BinarySerializer.BytesToObject<float>(buffer, ref head),
				                                             BinarySerializer.BytesToObject<float>(buffer, ref head));
			FeedAxis(newCoordinates.x, MouseAxis.Horizontal,   relCoord);
			FeedAxis(newCoordinates.y, MouseAxis.Vertical,     relCoord);
			FeedAxis(newCoordinates.z, MouseAxis.Depth_Scroll, relCoord);
		}
	}
	public enum MouseAxis : byte
	{
		Horizontal,
		Vertical,
		Depth_Scroll
	}
}