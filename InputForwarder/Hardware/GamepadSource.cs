namespace InputForwarder.Hardware
{
	using System.Collections.Generic;
	using ENet;
	using LiteNetLib;
	using SharpDX.XInput;



	public class GamepadSource
	{
		public Gamepad State
		{
			get => _state;
			set
			{
				if( _state.IsEqual(value) )
					return;

				var prev = _state;
				_state = value;
				OnStateChanged?.Invoke(this, prev, value);
			}
		}


		public readonly NetPeer Peer;
		public readonly int SourceSlot;
		
		public event System.Action<GamepadSource, Gamepad, Gamepad> OnStateChanged;
		
		Gamepad _state;



		public GamepadSource( NetPeer peer, int sourceSlot )
		{
			Peer = peer;
			SourceSlot = sourceSlot;
		}
		
		public GamepadSource( int sourceSlot )
		{
			SourceSlot = sourceSlot;
		}



		public override string ToString()
		{
			return $"{Peer.EndPoint.Address} gamepad #{SourceSlot}";
		}



		public static IEnumerable<GamepadSource> ExistingSources()
		{
			foreach( var source in HardwareInterface.GAMEPADS )
				yield return source;
			foreach( var source in RemoteGamepads.SampleRegisteredSources() )
				yield return source;
		}
	}
}