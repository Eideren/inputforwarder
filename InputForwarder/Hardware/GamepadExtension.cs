﻿namespace InputForwarder.Hardware
{
	using System.Collections.Generic;
	using SharpDX.XInput;
	using SimWinInput;



	public static class GamepadExtension
	{
		public static bool IsEqual( this Gamepad a, Gamepad b )
		{
			return a.Buttons == b.Buttons &&
			a.LeftTrigger == b.LeftTrigger &&
			a.RightTrigger == b.RightTrigger &&
			a.LeftThumbX == b.LeftThumbX &&
			a.LeftThumbY == b.LeftThumbY &&
			a.RightThumbX == b.RightThumbX &&
			a.RightThumbY == b.RightThumbY;
		}



		public static string Print( this Gamepad a )
		{
			return 
				$@"Sticks: ({Norm(a.LeftThumbX):F1},{Norm(a.LeftThumbY):F1}) ({Norm(a.RightThumbX):F1},{Norm(a.RightThumbY):F1})
Trigger: ({NormB(a.LeftTrigger):F1},{NormB(a.RightTrigger):F1}) {a.Buttons}";

			static float Norm(short val) => ((float)val) / short.MaxValue;
			static float NormB(byte val) => ((float)val) / byte.MaxValue;
		}



		public static Gamepad MergeStates( this IEnumerable<Gamepad> sources )
		{
			var dest = default(Gamepad);
			foreach( var source in sources )
			{
				dest.Buttons |= source.Buttons;
				dest.LeftTrigger = System.Math.Max(source.LeftTrigger, dest.LeftTrigger);
				dest.RightTrigger = System.Math.Max(source.RightTrigger, dest.RightTrigger);
				dest.LeftThumbX = Longest(source.LeftThumbX, dest.LeftThumbX);
				dest.LeftThumbY = Longest(source.LeftThumbY, dest.LeftThumbY);
				dest.RightThumbX = Longest(source.RightThumbX, dest.RightThumbX);
				dest.RightThumbY = Longest(source.RightThumbY, dest.RightThumbY);
			}

			return dest;
		}
		
		static short Longest( short a, short b ) => Abs( a ) > Abs( b ) ? a : b;
		
		static short Abs( short a ) => a < 0 ? a == short.MinValue ? short.MaxValue : (short)-a : a;


		public static void FromSharpDX( this SimulatedGamePadState dest, in Gamepad gamepad )
		{
			dest.Buttons = (GamePadControl)gamepad.Buttons;
			dest.LeftTrigger = gamepad.LeftTrigger;
			dest.RightTrigger = gamepad.RightTrigger;
			dest.LeftStickX = gamepad.LeftThumbX;
			dest.LeftStickY = gamepad.LeftThumbY;
			dest.RightStickX = gamepad.RightThumbX;
			dest.RightStickY = gamepad.RightThumbY;
		}
		
		
		
		public static unsafe T Read<T>(this byte[] bytes, ref int head) where T : unmanaged
		{
			T output = default;
			var ptr = (byte*)&output;
			for( int i = 0; i < sizeof(T); i++ )
				ptr[i] = bytes[head++];
			return output;
		}
	}
}