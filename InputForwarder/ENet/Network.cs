﻿#if UNITY_4 || UNITY_5 || UNITY_5_3_OR_NEWER
#define USING_UNITY
#endif
#define USING_DLOG
//#define DETAILED_LOG

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using InputForwarder;
using IPAddress = System.Net.IPAddress;

using LiteNetLib;
using LiteNetLib.Utils;
using ThreadPriority = System.Threading.ThreadPriority;



namespace ENet
{
    public class Network
    {
        public NetworkState networkState = NetworkState.NonInit;
        public string networkPassword { get; private set; }

        public NetManager netTransport;
        EventBasedNetListener       listener;
        readonly OngoingConnections ongoingConnections = new OngoingConnections();
        /// <summary>
        /// Non-essential work only, it is Abort()ed on shutdown
        /// </summary>
        Thread networkThread;


        public readonly Stopwatch networkLifetime    = new Stopwatch();
        public          double    lastSyncDurationMS = 0;


        Network()
        {
        }


        ~Network()
        {
            try
            {
                Shutdown("Destroying Network Instance");
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }
        }




        #region NET_FUNCTIONS

        async Task<bool> Startup(uint maxPeerCount, int netPort, string newNetworkPassword)
        {
            try
            {
                switch (networkState)
                {
                    case NetworkState.NonInit:
                        break;
                    case NetworkState.Ready:
                    case NetworkState.ShutDown:
                    case NetworkState.ShuttingDown:
                    case NetworkState.StartedUp:
                    case NetworkState.Starting:
                        throw new InvalidOperationException("Starting up already started up network is not allowed, shut it down properly and start another instance if required");
                    default:
                        throw new NotImplementedException("Network State " + networkState);
                }
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }

            networkState = NetworkState.Starting;
            OnStartingUp ? .Invoke();
            try
            {
                networkPassword = newNetworkPassword;
                d.log(netPort);
                if (IsPortAvailableForHosting(netPort) == false)
                    throw new NetworkException("Invalid port");



                listener     = new EventBasedNetListener();
                netTransport = new NetManager(listener)
                {
                    UnconnectedMessagesEnabled = true,
                    UnsyncedEvents             = true,
                    NatPunchEnabled            = true,
                };
                netTransport.Start(netPort);


                EventBasedNatPunchListener ebnpl = new EventBasedNatPunchListener();
                ebnpl.NatIntroductionRequest += OnNatIntroductionRequest;
                ebnpl.NatIntroductionSuccess += OnNatIntroductionSuccess;
                netTransport.NatPunchModule.Init(ebnpl);


                listener.PeerConnectedEvent             += OnPeerConnected;
                listener.PeerDisconnectedEvent          += OnPeerDisconnected;
                listener.NetworkErrorEvent              += OnTransportError;
                listener.NetworkReceiveUnconnectedEvent += OnTransportReceiveUnconnected;
                listener.NetworkReceiveEvent            += OnTransportReceive;
                listener.ConnectionRequestEvent         += OnReceivedConnectionRequest;
                //listener.NetworkLatencyUpdateEvent += TransportLatencyUpdate;


                networkThread = new Thread(NetworkThreadPolling)
                {
                    Priority = NetworkThreadPriority
                };
                networkThread.Start();


                networkState = NetworkState.StartedUp;
                OnStartedUp ? .Invoke();
                networkState = NetworkState.Ready;
                networkLifetime.Start();
                return true;
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }
        }


        void OnNatIntroductionSuccess (IPEndPoint targetEndPoint, NatAddressType type, string token)
        {
            NetLogWarning($"{nameof(OnNatIntroductionSuccess)} {targetEndPoint} {token}");
        }


        void OnNatIntroductionRequest (IPEndPoint localEndPoint, IPEndPoint remoteEndPoint, string token)
        {
            NetLogWarning($"{nameof(OnNatIntroductionRequest)} {localEndPoint} {remoteEndPoint} {token}");
        }


        void Shutdown(string reason)
        {
            try
            {
                switch (networkState)
                {
                    case NetworkState.NonInit:
                        throw new InvalidOperationException("Shutting down while not init");
                    case NetworkState.ShutDown:
                    case NetworkState.ShuttingDown:
                        return;
                    case NetworkState.StartedUp:
                    case NetworkState.Starting:
                    case NetworkState.Ready:
                        break;
                    default:
                        throw new NotImplementedException("Network State " + networkState);
                }
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }

            networkState                 = NetworkState.ShuttingDown;
            Exception delayableException = null;
            try
            {
                OnShuttingDown ? .Invoke(reason);

                networkThread ? .Abort(reason);
                networkThread = null;

                if (netTransport != null)
                {
                    netTransport.DisconnectAll();
                    netTransport ? .Stop();
                    netTransport = null;
                }

                if (instance == this)
                    instance = null;
            }
            catch (Exception e)
            {
                // Don't force crash if we are aborting
                if (e is System.Threading.ThreadAbortException)
                {
                    delayableException = e;
                }
                else
                {
                    ForceCrash(e);
                    throw;
                }
            }
            networkState = NetworkState.ShutDown;
            OnShutdown ? .Invoke(reason);
            Log ? .Invoke("Full shutdown successfull !");

            if (delayableException != null)
                throw delayableException;
        }


        async Task<ConnectionTask> ConnectTo(string host, int port)
        {
            try
            {
                switch (networkState)
                {
                    case NetworkState.NonInit:
                    case NetworkState.ShutDown:
                    case NetworkState.ShuttingDown:
                    case NetworkState.StartedUp:
                    case NetworkState.Starting:
                        throw new InvalidOperationException("Connecting while in not acceptable state " + networkState);
                    case NetworkState.Ready:
                        break;
                    default:
                        throw new NotImplementedException("Network State " + networkState);
                }
                return await ongoingConnections.CreateOrGetConnection(host, port, () => netTransport.Connect(new IPEndPoint(IPAddress.Parse(host), port), networkPassword) );
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                return new ConnectionTask() { result = ConnectionTask.Result.Failed };
            }
        }


        async Task<ConnectionTask> ConnectTo(ulong GUID)
        {
            // TODO : NAT_TRAVERSAL
            throw new NotImplementedException();
            /*
            try
            {
                switch (networkState)
                {
                    case NetworkState.NonInit:
                    case NetworkState.ShutDown:
                    case NetworkState.ShuttingDown:
                    case NetworkState.StartedUp:
                    case NetworkState.Starting:
                        throw new InvalidOperationException("Connecting while in not acceptable state " + networkState);
                    case NetworkState.Ready:
                        break;
                    default:
                        throw new NotImplementedException("Network State " + networkState);
                }


                if (natHelper == null)
                {
                    Warning ? .Invoke("Nat Helper Missing");
                    return false;
                }


                // Should use the same await pattern as standard ip+port connection does
                // StaticUnityInterface.Main_ASAP += () => natHelper.StartCoroutine(natHelper.punchThroughToServer(GUID, onHolePunchedClient));
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                return false;
            }*/
        }

        #endregion




        #region COMMUNICATION

        public static void NetLog(object s)
        {
            Log ? .Invoke(s.ToString());
        }



        public static void NetDoFatalError(Exception e)
        {
            OnFatalError ? .Invoke(e);
        }



        public static void NetLogWarning(string s)
        {
            Warning ? .Invoke(s);
        }

        #endregion




        #region CONNECTION_EVENTS


        void OnPeerConnected(NetPeer peer)
        {
            try
            {
                ConnectionDirection connectionDirection = ongoingConnections.ClearConnectionRequest(peer.EndPoint.Address.ToString(), peer.EndPoint.Port, ConnectionTask.Result.Successful);

                OnConnectionEstablished ? .Invoke(peer, connectionDirection);

                Log ? .Invoke(nameof(OnPeerConnected));
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }
        }

        void OnReceivedConnectionRequest(ConnectionRequest request)
        {
            ongoingConnections.NotifyOfConnectionRequest(request.RemoteEndPoint.Address.ToString(), request.RemoteEndPoint.Port);
            Log ? .Invoke("Received Connection Request");
            if (request.Data.GetString() == networkPassword)
                request.Accept();
            else
                request.Reject();
        }

        void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            try
            {
                OnPeerDisconnect ? .Invoke(peer, disconnectInfo);
                Log ? .Invoke($"{nameof(OnPeerDisconnected)}{disconnectInfo.Reason}");
                ongoingConnections.ClearConnectionRequest(peer.EndPoint.Address.ToString(), peer.EndPoint.Port, ConnectionTask.Result.Failed, disconnectInfo);
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }
        }





        void OnTransportError(IPEndPoint endPoint, SocketError socketError)
        {
            OnFatalError ? .Invoke(new NetworkException($"Net Transport error {socketError}"));
            Log ? .Invoke(nameof(OnTransportError));
        }

        #endregion




        #region UPDATE_EVENTS

        /*void TransportLatencyUpdate(NetPeer nPeer, int latency)
        {
            try
            {
                Log ? .Invoke($"l:{latency} / p:{nPeer.Ping}");
                Peer peer;
                if (Peer.TryGet(nPeer, out peer))
                    peer.latency = latency;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Warning ? .Invoke(e.Message);
            }
        }*/



        void OnTransportReceive(NetPeer peer, NetPacketReader reader, byte channel, DeliveryMethod deliveryMethod)
        {
            try
            {
                PacketIO.ReadPacket(this, peer, reader, deliveryMethod);
            }
            catch (Exception e)
            {
                OnFatalError ? .Invoke(e);
                throw;
            }
        }



        void OnTransportReceiveUnconnected(IPEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            // Ignore any messages sent by ourselves directly
            if (remoteEndPoint.Port == Info.port)
            {
                string rawHost = remoteEndPoint.Address.ToString();
                // Remove windows's IPv6 scope Id
                if (rawHost.Contains('%'))
                    rawHost = rawHost.Substring(0, rawHost.IndexOf('%'));
                IPAddress remote = IPAddress.Parse(rawHost);
                if (System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces().SelectMany(item => item.GetIPProperties().UnicastAddresses).Any(ip => remote.Equals(ip.Address)))
                    return;
            }
            Log ? .Invoke(nameof(OnTransportReceiveUnconnected)+":"+messageType);
        }

        #endregion





        #region NETPOLLING

        static readonly object _netSyncMonitorLock = new object();
        TimeSpan lastSync;
        /// <summary>
        /// Ask Network to sync ASAP by waking up the sync thread
        /// </summary>
        public static void WakeupSync()
        {
            lock (_netSyncMonitorLock)
            {
                Monitor.Pulse(_netSyncMonitorLock);
            }
        }

        void NetworkThreadPolling()
        {
            try
            {
                // Wait for the network to be entirely started before polling
                while (networkState == NetworkState.Starting || networkState == NetworkState.StartedUp)
                {
                    Thread.Sleep(100);
                }
                lock (_netSyncMonitorLock)
                {
                    Stopwatch packetProcessingChrono = new Stopwatch();
                    while (networkState == NetworkState.Ready)
                    {
                        try
                        {
                            // How much time passed since the last sync
                            TimeSpan deltaTime = networkLifetime.Elapsed.Subtract(lastSync);
                            if (deltaTime < Config.syncTimespanOffset)
                            {
                                // Sleep just long enough for this thread to wake up on syncTime or wake up when an external resource Pulsed this lock
                                Monitor.Wait(_netSyncMonitorLock, Config.syncTimespanOffset.Subtract(deltaTime));
                            }

                            if (netTransport.UnsyncedEvents == false)
                                netTransport.PollEvents();
                            // Add offset instead of setting it to ensure that we sync exactly at the rate specified
                            lastSync = lastSync.Add(Config.syncTimespanOffset);
                            packetProcessingChrono.Restart();
                            SyncLoop();
                            lastSyncDurationMS = packetProcessingChrono.Elapsed.TotalMilliseconds;
                            // happens sporadicly, doesn't seem to be based on my code
                            /*if( packetProcessingChrono.Elapsed.TotalMilliseconds >= Config.syncMilliOffset )
                                Warning ? .Invoke($"Serialization took longer than a tick (+{packetProcessingChrono.Elapsed.Subtract(Config.syncTimespanOffset).TotalMilliseconds}ms)");*/
                        }
                        catch (ThreadAbortException e)
                        {
                            Task.Run(() => ShutdownNetwork(e.ToString()));
                            return;
                        }
                        catch (Exception e)
                        {
                            OnFatalError ? .Invoke(e);
                            throw;
                        }
                    }
                }
            }
            catch (ThreadAbortException e)
            {
                Task.Run(() => ShutdownNetwork(e.ToString()));
                return;
            }
            Log ? .Invoke("Thread shutdown");
        }



        /// <summary>
        /// Use only inside SyncLoop
        /// </summary>
        readonly ByteBuffer cachedBuffer = new ByteBuffer(64, 16);
        readonly List<NetPeer> peerBuffer = new List<NetPeer>();
        void SyncLoop()
        {
            // Don't bother if there aren't any peers
            // TODO: uncomment line below when we aren't debugging?
            /*if(Peer.Count == 0)
                return;*/
            OnPreSync ? .Invoke();
            cachedBuffer.Clear();
            netTransport.GetPeersNonAlloc( peerBuffer, ConnectionState.Connected );
            PacketIO.SerializeCommandsAndSendPacket( peerBuffer, cachedBuffer );
            OnPostSync ? .Invoke();
        }



        #endregion





        #region EVENTS

        /// <summary>
        /// Runs between state Starting and state StartedUp ; before the network transport setup.
        /// </summary>
        public static event Action OnStartingUp;

        /// <summary>
        /// Runs between state StartedUp and state Ready; after the network transport is done setting up. Doesn't run if the network encountered an error while booting up.
        /// </summary>
        public static event Action OnStartedUp;

        /// <summary>
        /// IF AN ERROR IS ENCOUNTERED INSIDE THIS EVENT THE APPLICATION WILL FORCE CRASH ! <br/>
        /// Runs between state ShuttingDown and state ShutDown; runs before network transport shutdowns, shutdown will occur when encountering an error when booting up, and is called even if NetworkStartedUp couldn't.
        /// </summary>
        public static event Action<string> OnShuttingDown;

        /// <summary>
        /// Runs after state ShutDown ; shutdown will occur when encountering an error when booting up, and is called even if NetworkStartedUp couldn't.
        /// </summary>
        public static event Action<string> OnShutdown;

        /// <summary>
        /// Runs right after the connection is established, use Peer
        /// </summary>
        public static event Action<NetPeer, ConnectionDirection> OnConnectionEstablished;

        /// <summary>
        /// Runs when the network is shutdown and when a specific peer is disconnected
        /// </summary>
        public static event Action<NetPeer, DisconnectInfo> OnPeerDisconnect;

        /// <summary>
        /// Runs when a fatal to network error has been detected
        /// </summary>
        public static event Action<string> Warning;

        /// <summary>
        /// Runs when network outputs a non-warning and non-fatal Log
        /// </summary>
        public static event Action<string> Log;

        /// <summary>
        /// Runs when a fatal to network error has been detected, will automatically shutdown the network when called
        /// </summary>
        public static event Action<Exception> OnFatalError;

        /// <summary>
        /// Runs when a fatal to application error has been detected
        /// </summary>
        public static event Action<Exception> OnForcedApplicationCrash;

        /// <summary>
        /// Runs before we start a Sync
        /// </summary>
        public static event Action OnPostSync;

        /// <summary>
        /// Runs after we finished a Sync
        /// </summary>
        public static event Action OnPreSync;

        #endregion


        static Task pendingNetworkTask = Task.Run(() => true);

        const ThreadPriority NetworkThreadPriority = ThreadPriority.Normal;

        public static Network instance { get; private set; }

        static Network()
        {
            OnFatalError += (e) => ShutdownNetwork(e.ToString());
            #if USING_UNITY
                Log += Debug.Log;
                Warning += Debug.LogWarning;
                OnFatalError += (s) => Debug.LogError($"{nameof(OnFatalError)}:{s}");

                OnShuttingDown += (s) => Debug.Log($"{nameof(OnShuttingDown)}:{s}");
                OnShutdown += (s) => Debug.Log($"{nameof(OnShutdown)}:{s}");

                OnForcedApplicationCrash += (s) => Debug.LogError($"Manually forced stackoverflow crash ! Program couldn't continue running safely under a fatal error:{s}");

                OnDiscoveryResponse += (s) => Debug.LogError($"{nameof(OnDiscoveryResponse)}:{s}");

                #if DETAILED_LOG
                StartingUp += () => Debug.Log(nameof(StartingUp));
                StartedUp += () => Debug.Log(nameof(StartedUp));
                OnWeConnectedToRemote += (p) => Debug.Log($"{nameof(OnWeConnectedToRemote)}:{p}");
                OnRemoteConnectedToUs += (p) => Debug.Log($"{nameof(OnRemoteConnectedToUs)}:{p}");
                PostSync += () => Debug.Log($"{nameof(PostSync)}");
                PreSync += () => Debug.Log($"{nameof(PreSync)}");
                #endif
            #elif USING_DLOG
                Log          += d.log;
                Warning      += d.logwarning;
                OnFatalError += (s) => d.logerror($"{nameof(OnFatalError)}:{s}");

                OnShuttingDown += (s) => d.log($"{nameof(OnShuttingDown)}:{s}");
                OnShutdown     += (s) => d.log($"{nameof(OnShutdown)}:{s}");

                OnForcedApplicationCrash += (s) => d.logerror($"Manually forced stackoverflow crash ! Program couldn't continue running safely under a fatal error:{s}");

                #if DETAILED_LOG
                    StartingUp += () => d.logerror(nameof(StartingUp));
                    StartedUp += () => d.logerror(nameof(StartedUp));
                    OnWeConnectedToRemote += (p) => d.logerror($"{nameof(OnWeConnectedToRemote)}:{p}");
                    OnRemoteConnectedToUs += (p) => d.logerror($"{nameof(OnRemoteConnectedToUs)}:{p}");
                    PostSync += () => d.logerror($"{nameof(PostSync)}");
                    PreSync += () => d.logerror($"{nameof(PreSync)}");
                #endif
            #else
                #error NoLoggerImplemented
            #endif
        }
        static void ForceCrash(Exception e)
        {
            OnForcedApplicationCrash ? .Invoke(e);
            GenerateStackoverflow();
        }
        static void GenerateStackoverflow(int i = 0)
        {
            GenerateStackoverflow(i++);
        }








        #region STATIC_LOGIC

        static async Task WaitForPendingTask()
        {
            if (pendingNetworkTask.IsCompleted == false)
            {
                try { await pendingNetworkTask; }
                catch { /* Task source is already taking care of it */ }
            }
        }

        /// <summary>
        /// Start a new network, will throw exceptions and run the NetworkFatalError() event if failed,
        /// if a network is already running it will shut it down and start a new one using the parameters given.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static async Task<Network> StartNetwork(uint maxPeerCount, ushort port, string networkPassword)
        {
            await WaitForPendingTask();

            if (Info.state == NetworkState.Ready)
                await ShutdownNetwork("Called StartNetwork when network was already running -> Restarting network");

            //Network newNet = new Network();
            instance = new Network();
            await (pendingNetworkTask = Task.Run(() => /*newNet.*/instance.Startup(maxPeerCount, port, networkPassword)));
            //instance = newNet;
            return instance;
        }

        /// <summary>
        /// Connect to peer, will throw exception if you didn't start the network before hand
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static async Task<ConnectionTask> ConnectToNetwork(string ip, int port)
        {
            await WaitForPendingTask();

            if(Info.state != NetworkState.Ready)
                throw new InvalidOperationException("Network not running");

            return await instance.ConnectTo(ip, port);
        }

		public static async Task<ConnectionTask> ConnectToNetwork(ulong GUID)
		{
		    await WaitForPendingTask();

			if(Info.state != NetworkState.Ready)
				throw new InvalidOperationException("Network not running");

			return await instance.ConnectTo(GUID);
		}

        /// <summary>
        /// You can call this method anytime, if it is already shutdown it will return directly.
        /// </summary>
        public static async Task ShutdownNetwork(string cause)
        {
            await WaitForPendingTask();

            if (instance == null || Info.state == NetworkState.ShutDown || Info.state == NetworkState.NonInit)
                return;

            instance ? .Shutdown(cause);
            instance = null;
        }

        #endregion





        #region UTILITY


        public static bool IsPortAvailableForHosting(int port)
        {
            try
            {
                using (Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                    sock.Bind(new IPEndPoint(IPAddress.Any, port));
                    if (((IPEndPoint) sock.LocalEndPoint).Port == 0)
                        return false;
                    sock.Close();
                }
            }
            catch
            {
                return false;
            }
            return true;

        }
        #endregion





        public static class Info
        {
            public static NetworkState state => instance ? .networkState ?? NetworkState.NonInit;

            public static int port => instance ? .netTransport.LocalPort ?? -1;

            public static long totalBytesSent => instance ? .netTransport ? .Statistics ? .BytesSent ?? 0;
            public static double lifetimeSeconds => instance ? .networkLifetime ? .Elapsed.TotalSeconds ?? 0;
            public static double packetProcessingDurationMS => instance ? .lastSyncDurationMS ?? 0;

            public const ushort defaultPort = 51423;
            public static readonly IPAddress defaultIP = IPAddress.Parse("127.0.0.1");
            public const uint defaultMaxPeer = 255;
            public const string defaultPassword = "defaultPassword";

            /// <summary>
            /// Can functions be called safely over network
            /// </summary>
            /// <returns></returns>
            public static bool IsInsideCallableState => state == NetworkState.Ready || state == NetworkState.StartedUp;

            public static void FetchLatencyInfo(out double average, out int max)
            {
                int count, totalLatency;
                max = count = totalLatency = 0;
                List<NetPeer> peers = instance ? .netTransport ? .ConnectedPeerList;
                if (peers != null)
                {
                    foreach (NetPeer remotePeer in peers)
                    {
                        count++;
                        int latency = remotePeer.Ping;
                        totalLatency += latency;
                        if (latency > max)
                            max = latency;
                    }
                }
                average = count == 0 ? 0d : (double)totalLatency / count;
            }
        }




        public static class Config
        {
            public static uint syncTickrate = 60;
            public static double syncMilliOffset => 1000d / (double)syncTickrate;
            public static TimeSpan syncTimespanOffset => TimeSpan.FromMilliseconds(syncMilliOffset);
        }





        class OngoingConnections
        {
            readonly ConcurrentDictionary<string, OngoingConnection> CONNECTION_REQUEST_SENT = new ConcurrentDictionary<string, OngoingConnection>();
            readonly ConcurrentDictionary<string, byte>              CONNECTION_REQUEST_RECEIVED = new ConcurrentDictionary<string, byte>();





            public async Task<ConnectionTask> CreateOrGetConnection(string host, int port, Func<NetPeer> connectionTask)
            {
                return await CreateOrGetConnection(GenerateConnectionId(host, port), instance, connectionTask);
            }

            async Task<ConnectionTask> CreateOrGetConnection(string connectionId, Network instance, Func<NetPeer> connectionTask)
            {
                OngoingConnection connection;
                bool shouldInitConn;


                {
                    OngoingConnection newConnection = new OngoingConnection(connectionId);
                    OngoingConnection dictionaryConnection = CONNECTION_REQUEST_SENT.GetOrAdd(connectionId, newConnection);

                    shouldInitConn = ReferenceEquals(newConnection, dictionaryConnection);
                    connection = dictionaryConnection;
                }


                if (shouldInitConn)
                {
                    connection.wHandle = new AutoResetEvent(false);
                    return await (connection.systemTask = Task.Run(() =>
                    {
                        NetPeer np = connectionTask();
                        if(np ? .ConnectionState.HasFlag(ConnectionState.Connected) ?? false)
                        {
                            ClearConnectionRequest(connection.key, ConnectionTask.Result.AlreadyConnected);
                            connection.wHandle.Set();
                        }
                        Log ? .Invoke("Waiting for connection");
                        connection.wHandle.WaitOne();
                        Log ? .Invoke($"Connection finished with result {connection.task.result}");
                        return connection.task;
                    }));
                }
                else
                    return await connection.systemTask;
            }







            static string GenerateConnectionId(string host, int port)
            {
                return $"{host}|{port}";
            }

            public ConnectionDirection ClearConnectionRequest(string host, int port, ConnectionTask.Result result, object disconnectInfo = null)
            {
                return ClearConnectionRequest(GenerateConnectionId(host, port), result, disconnectInfo);
            }

            ConnectionDirection ClearConnectionRequest(string key, ConnectionTask.Result result, object disconnectInfo = null)
            {
                ConnectionDirection direction = CONNECTION_REQUEST_RECEIVED.ContainsKey(key) ? CONNECTION_REQUEST_SENT.ContainsKey(key) ? ConnectionDirection.Both : ConnectionDirection.RemoteToUs : ConnectionDirection.UsToRemote;
                OngoingConnection oConnection;
                if (CONNECTION_REQUEST_SENT.TryRemove(key, out oConnection))
                {
                    switch (result)
                    {
                        case ENet.ConnectionTask.Result.AlreadyConnected:
                            oConnection.task.ReportAlreadyConnected();
                            break;
                        case ENet.ConnectionTask.Result.Failed:
                            if(disconnectInfo == null)
                                throw new ArgumentException($"Failed but {nameof(disconnectInfo)} == null");
                            DisconnectInfo dInfo = (DisconnectInfo) disconnectInfo;
                            oConnection.task.ReportFail(dInfo);
                            break;
                        case ENet.ConnectionTask.Result.Successful:
                            oConnection.task.ReportSuccess();
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    oConnection.wHandle.Set();
                }
                byte b;
                CONNECTION_REQUEST_RECEIVED.TryRemove(key, out b);

                return direction;
            }


            public void NotifyOfConnectionRequest(string host, int port)
            {
                NotifyOfConnectionRequest(GenerateConnectionId(host, port));
            }

            void NotifyOfConnectionRequest(string key)
            {
                CONNECTION_REQUEST_RECEIVED.TryAdd(key, 0);
            }


            class OngoingConnection
            {
                public readonly string         key;
                public readonly ConnectionTask task       = new ConnectionTask();
                public Task<ConnectionTask>    systemTask = null;
                public AutoResetEvent          wHandle;


                public OngoingConnection(string newKey)
                {
                    key = newKey;
                }
            }
        }
    }

    public enum NetworkState
    {
        NonInit,
        Starting,
        StartedUp,
        Ready,
        ShuttingDown,
        ShutDown
    }




    public enum ConnectionDirection
    {
        /// <summary>
        /// We sent the connection request
        /// </summary>
        UsToRemote,
        /// <summary>
        /// We received the connection request
        /// </summary>
        RemoteToUs,
        /// <summary>
        /// Both sent their request, we received the remote request first
        /// </summary>
        Both
    }

    public class NetworkException : Exception
    {
        public readonly string message;
        public NetworkException(){}

        public NetworkException(string newMessage)
            : base(newMessage)
        {
            message = newMessage;
        }

        public NetworkException(string newMessage, Exception inner)
            : base(newMessage, inner)
        {
            message = newMessage;
        }
    }
    public class ConnectionTask
    {
        public Result result = Result.Waiting;
        public DisconnectInfo disconnectInfo;



        public void ReportFail(DisconnectInfo newDisconnectInfo)
        {
            if(result != Result.Waiting)
                throw new AccessViolationException("Setting a result already set");
            disconnectInfo = newDisconnectInfo;
            result = Result.Failed;
        }


        public void ReportAlreadyConnected()
        {
            if(result != Result.Waiting)
                throw new AccessViolationException("Setting a result already set");
            result = Result.AlreadyConnected;
        }

        public void ReportSuccess()
        {
            if(result != Result.Waiting)
                throw new AccessViolationException("Setting a result already set");
            result = Result.Successful;
        }

        public override string ToString()
        {
            return $"{result}{ (result == Result.Failed ? disconnectInfo.Reason.ToString() : string.Empty) }";
        }

        public enum Result
        {
            Failed,
            Waiting,
            Successful,
            AlreadyConnected
        }
    }
}