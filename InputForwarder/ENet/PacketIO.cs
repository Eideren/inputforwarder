﻿using System;
using LiteNetLib;
using System.Collections.Concurrent;
using System.Collections.Generic;
using InputForwarder;
using InputForwarder.Hardware;







namespace ENet
{
	using SharpDX.XInput;



	public static class PacketIO
	{
		public static bool immediateMode = true;

		static readonly ConcurrentQueue<Command> commandsUnreliable = new ConcurrentQueue<Command>();
		static readonly ConcurrentQueue<Command> commandsReliableUnordered = new ConcurrentQueue<Command>();

		[ThreadStatic]
		static List<CommandType> history;
		public static void ReadPacket(Network network, NetPeer peer, LiteNetLib.Utils.NetDataReader netReader, DeliveryMethod deliveryMethod)
		{
			int head = netReader.Position;
			byte[] buffer = netReader.RawData;
			try
			{
				if(history == null)
					history = new List<CommandType>();
				history.Clear();
				while (head < buffer.Length)
				{
					CommandType reader = (CommandType)buffer[head++];
					history.Add(reader);
					switch (reader)
					{
						case CommandType.KeyboardState:
							HardwareInterface.KEYBOARD_STATE.Feed(buffer, ref head);
							break;
						case CommandType.GamepadState:
							DealWithGamepadStateCommand(peer, buffer, ref head);
							break;
						case CommandType.MouseButton:
							DealWithMouseButtonCommand(peer, buffer, ref head);
							break;
						case CommandType.MouseAxis:
							DealWithMouseAxisCommand(peer, buffer, ref head);
							break;
						default:
							throw new PacketReadingException();
					}
				}
			}
			catch (Exception e)
			{
				if (e is PacketReadingException)
				{
					byte b = buffer[head - 1];
					d.logerror($"Invalid/unimplemented command type, couldn't safely read packet, byte representation: {b} / { (CommandType)b} @ {head-1}\nHistory:{string.Join(", ", history)}");
				}
				else
					d.logerror(e);
			}
		}







		public static void SerializeCommandsAndSendPacket(List<NetPeer> peers, ByteBuffer cleanBuffer)
		{
			if (Network.Info.state != NetworkState.Ready)
			{
				Network.NetLogWarning("Sending while network isn't properly init yet");
				return;
			}

			while (commandsUnreliable.Count > 0)
			{
				if (commandsUnreliable.TryDequeue(out Command cmd) == false)
					continue;
				cleanBuffer.Add((byte)cmd.type);
				cmd.serialization(cleanBuffer);
			}
			SendToAll(peers, cleanBuffer, DeliveryMethod.Unreliable);

			cleanBuffer.Clear();
			while (commandsReliableUnordered.Count > 0)
			{
				bool send = commandsReliableUnordered.TryDequeue(out Command cmd);
				if (send)
				{
					cleanBuffer.Add((byte)cmd.type);
					cmd.serialization(cleanBuffer);
				}
			}

			SendToAll(peers, cleanBuffer, DeliveryMethod.ReliableUnordered);
		}


		static void SendToAll(List<NetPeer> peers, ByteBuffer filledBuffer, DeliveryMethod method)
		{
			if(filledBuffer.Length == 0)
				return;
			byte[] output = filledBuffer.TrimAndOutput();
			try
			{
				foreach (NetPeer peer in peers)
				{
					peer.Send( output, method );
				}
			}
			catch (Exception e)
			{
				d.logwarning(e);
			}
		}










		static void NewCommand(Command command, DeliveryMethod deliveryMethod, bool forceImmediate = false)
		{
			switch (deliveryMethod)
			{
				case DeliveryMethod.Unreliable:
					commandsUnreliable.Enqueue(command);
					break;
				case DeliveryMethod.ReliableUnordered:
					commandsReliableUnordered.Enqueue(command);
					break;
				default:
					throw new NotImplementedException(deliveryMethod.ToString());
			}
			if(immediateMode || forceImmediate)
				Network.WakeupSync();
		}


		public static void NewKeyboardStateCommand(bool forceImmediate = false)
		{
			NewCommand( new Command(CommandType.KeyboardState, buffer => HardwareInterface.KEYBOARD_STATE.Serialize(buffer)), DeliveryMethod.Unreliable, forceImmediate );
		}





		public static void NewMouseButtonCommand(bool forceImmediate = false)
		{
			NewCommand( new Command(CommandType.MouseButton, buffer =>
			{
				HardwareInterface.MOUSE_STATE.SerializeButtons(buffer);
			}), DeliveryMethod.Unreliable, forceImmediate );
		}

		public static void NewMouseAxisCommand(bool forceImmediate = false)
		{
			NewCommand( new Command(CommandType.MouseAxis, buffer =>
			{
				HardwareInterface.MOUSE_STATE.SerializeAxis(buffer);
			}), DeliveryMethod.Unreliable, forceImmediate );
		}


		static void DealWithMouseButtonCommand(NetPeer peer, byte[] buffer, ref int head)
		{
			HardwareInterface.MOUSE_STATE.FeedButtons(buffer, ref head);
		}

		static void DealWithMouseAxisCommand(NetPeer peer, byte[] buffer, ref int head)
		{
			HardwareInterface.MOUSE_STATE.FeedAxis(buffer, ref head);
		}

		
		public static void NewGamepadStateCommand(Gamepad state, int gamepadIndex, bool forceImmediate = false)
		{
			NewCommand( new Command(CommandType.GamepadState, buffer =>
			{
				buffer.Add( (byte)gamepadIndex );
				buffer.Add(state);
			}), DeliveryMethod.Unreliable, forceImmediate );
		}
		
		static void DealWithGamepadStateCommand(NetPeer peer, byte[] buffer, ref int head)
		{
			int gamepadIndex = buffer[head++];
			Gamepad state = buffer.Read<Gamepad>(ref head);
			if( RemoteGamepads.NewStateFor( peer, state, gamepadIndex, out var source ) )
				GamepadMappings.AddNewMappingIfNone( source );
		}













		enum CommandType : byte
		{
			KeyboardState,
			GamepadState,
			MouseButton,
			MouseAxis
		}
		
		struct Command
		{
			public readonly CommandType type;
			public readonly Action<ByteBuffer> serialization;


			public Command(CommandType newType, Action<ByteBuffer> newSerialization)
			{
				type = newType;
				serialization = newSerialization;
			}
		}

		class PacketReadingException : Exception {}
	}





}