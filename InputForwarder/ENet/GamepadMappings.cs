﻿namespace ENet
{
	using System;
	using System.Linq;
	using SharpDX.XInput;
	using System.Collections.Generic;
	using InputForwarder.Hardware;
	using LiteNetLib;



	public static class GamepadMappings
	{
		public static event Action<Gamepad, int> GamepadStateChanged;
		public static event MappingEventDelegate MappingAdded, MappingRemoved;
		static readonly HashSet<Mapping> _mappings = new HashSet<Mapping>();
		static readonly object _lock = new object();
		
		public delegate void MappingEventDelegate(GamepadSource source, int mappedToIndex, int amountOfOtherSourceOnIndex);





		static GamepadMappings()
		{
			Network.OnPeerDisconnect += (p, i) => DisconnectPeer(p);



			void DisconnectPeer( NetPeer peer )
			{
				lock( _lock )
				{
					foreach( Mapping mapping in _mappings.ToArray() )
					{
						if( mapping.IsPeer == peer )
						{
							mapping.Dispose();
							_mappings.Remove( mapping );
						}
					}
				}
			}
		}



		public static void AddNewMappingIfNone( GamepadSource source )
		{
			int mask = 0;
			lock( _lock )
			{
				foreach( var mapping in _mappings )
				{
					if( mapping.Source == source )
						return;
					mask |= 1 << mapping.Index;
				}
			}

			int slot = -1;
			if( ( mask & 1 ) == 0 )
				slot = 0;
			else if( ( mask & 2 ) == 0 )
				slot = 1;
			else if( ( mask & 4 ) == 0 )
				slot = 2;
			else if( ( mask & 8 ) == 0 )
				slot = 3;
			if(slot != -1)
				TryAddMapping( source, slot );
		}



		public static List<(GamepadSource source, int indexMappedTo)> SampleRemoteMappings()
		{
			var sources = new List<(GamepadSource, int)>();
			lock( _lock )
			{
				foreach( var mapping in _mappings )
					sources.Add( ( mapping.Source, mapping.Index ) );
			}

			return sources;
		}



		public static void RemoveMappings(Func<(NetPeer peer, GamepadSource source, int index), bool> Remove)
		{
			lock( _lock )
			{
				foreach( var mapping in _mappings.ToArray() )
				{
					if( Remove(( mapping.IsPeer, mapping.Source, mapping.Index )) )
					{
						mapping.Dispose();
						_mappings.Remove( mapping );
					}
				}
			}
		}



		public static bool TryAddMapping( GamepadSource source, int index )
		{
			lock( _lock )
			{
				foreach( var mapping in _mappings )
				{
					if( mapping.IsPeer == source.Peer && mapping.Source == source && mapping.Index == index )
						return false;
				}
				_mappings.Add( new Mapping( source.Peer, source, index ) );
				return true;
			}
		}



		public static Gamepad SampleStateFor(int index) => Mapping.SampleStateFor( index );



		class Mapping : IDisposable
		{
			public readonly NetPeer IsPeer;
			public readonly GamepadSource Source;
			public readonly int Index;
			bool _disposed;
			
			public static readonly HashSet<Mapping>[] _mappingsPerSlot = new HashSet<Mapping>[4]
			{
				new HashSet<Mapping>(),
				new HashSet<Mapping>(),
				new HashSet<Mapping>(),
				new HashSet<Mapping>(),
			};



			public Mapping( NetPeer IsPeer, GamepadSource Source, int Index )
			{
				this.IsPeer = IsPeer;
				this.Source = Source;
				this.Index = Index;
				int mappingSlotCount;
				lock( _mappingsPerSlot )
				{
					var mappingsOnThisSlot = _mappingsPerSlot[ Index ];
					mappingSlotCount = mappingsOnThisSlot.Count;
					mappingsOnThisSlot.Add(this);
				}
				Source.OnStateChanged += SourceOnStateChanged;
				MappingAdded?.Invoke(this.Source, Index, mappingSlotCount);
			}



			void SourceOnStateChanged( GamepadSource arg1, Gamepad arg2, Gamepad arg3 )
			{
				var newState = SampleStateFor( Index );
				GamepadStateChanged?.Invoke(newState, Index);
			}



			public static Gamepad SampleStateFor(int index)
			{
				// When any gamepad changed, take all the gamepad state on the same slot, merge them together and simulate this new merged state
				lock( _mappingsPerSlot )
				{
					return _mappingsPerSlot[ index ].Select( x => x.Source.State ).MergeStates();
				}
			}



			~Mapping()
			{
				
				Dispose();
			}



			public void Dispose()
			{
				int mappingCount;
				lock( _mappingsPerSlot )
				{
					if(_disposed)
						return;
					_disposed = true;
					Source.OnStateChanged -= SourceOnStateChanged;
					var mappingsOnThisSlot = _mappingsPerSlot[ Index ];
					mappingsOnThisSlot.Remove(this);
					mappingCount = mappingsOnThisSlot.Count;
				}
				MappingRemoved?.Invoke( Source, Index, mappingCount );
			}
		}
	}
}