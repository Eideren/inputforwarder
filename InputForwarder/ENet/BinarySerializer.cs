﻿#if UNITY_4 || UNITY_5 || UNITY_5_3_OR_NEWER
#define USING_UNITY
#endif
//#define USING_VGENERICS

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using Convert = System.Convert;
using Array = System.Array;
using Activator = System.Activator;
using DateTime = System.DateTime;
using Type = System.Type;
using Enum = System.Enum;
using TypeCode = System.TypeCode;
using NotSupportedException = System.NotSupportedException;
using NotImplementedException = System.NotImplementedException;
using InvalidOperationException = System.InvalidOperationException;

#if USING_UNITY
using Debug = UnityEngine.Debug;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;
using Quaternion = UnityEngine.Quaternion;
#endif
using BS = ENet.BinarySerializer;
using Reflection = System.Reflection;





namespace ENet
{
	public static class BinarySerializer
	{
		public delegate void ToBytes(object obj, ByteBuffer buffer);
		public delegate object FromBytes(byte[] buffer, ref int head);
		public static Dictionary<Type, (ToBytes ToBytes, FromBytes FromBytes)> complexTypesConverters = new Dictionary<Type, (ToBytes, FromBytes)>
		{
			{ typeof(IPEndPoint), (EndPointConverter.ToBytes, EndPointConverter.FromBytes) }
		};

		public static Dictionary<Type, Type> enumValueTypeConversion = new Dictionary<Type, Type>();

		public static void ObjectToBytes_FObj(object obj, ByteBuffer buffer)
		{
			if(obj == null)
				throw new NotSupportedException("null values not-supported");

			Type objType = obj.GetType();
			if (obj is Enum)
			{
				if (enumValueTypeConversion.TryGetValue(objType, out Type valueType) == false)
				{
					valueType = Enum.GetUnderlyingType(objType);
					enumValueTypeConversion.Add(objType, valueType);
					if (valueType == typeof(int))
						Network.NetLogWarning($"Serializing to sub-optimal value type for enum '{objType}', consider specifying explicitly a smaller value type for that enum");
				}
				ObjectToBytes(Convert.ChangeType(obj, valueType), buffer);
				return;
			}
			else if (objType.IsValueType)
			{
				switch (obj)
				{
					case bool v:
						ObjectToBytes(v, buffer);
						return;
					case byte v:
						ObjectToBytes(v, buffer);
						return;
					case sbyte v:
						ObjectToBytes(v, buffer);
						return;
					case char v:
						ObjectToBytes(v, buffer);
						return;

					case ushort v:
						ObjectToBytes(v, buffer);
						return;
					case uint v:
						ObjectToBytes(v, buffer);
						return;
					case ulong v:
						ObjectToBytes(v, buffer);
						return;

					case short v:
						ObjectToBytes(v, buffer);
						return;
					case int v:
						ObjectToBytes(v, buffer);
						return;
					case long v:
						ObjectToBytes(v, buffer);
						return;

					case float v:
						ObjectToBytes(v, buffer);
						return;
					case double v:
						ObjectToBytes(v, buffer);
						return;

					case decimal v:
						ObjectToBytes(v, buffer);
						return;
					case DateTime v:
						ObjectToBytes(v, buffer);
						return;
					#if USING_UNITY
					case Vector2 v:
						ObjectToBytes(v, buffer);
						return;
					case Vector3 v:
						ObjectToBytes(v, buffer);
						return;
					case Vector4 v:
						ObjectToBytes(v, buffer);
						return;
					case Quaternion v:
						ObjectToBytes(v, buffer);
						return;
					#endif
					default:
						goto CONTINUE;
				}
			}
			CONTINUE:
			if (obj is IBinarySerializable<object> bseri)
			{
				bseri.ToBytes(buffer);
				return;
			}
			#if USING_VGENERICS
			else if (obj is VectorGenerics.IVectorGeneric vG)
			{
				for (int i = 0; i < vG.dimensions; i++)
				{
					ObjectToBytes(vG[i], buffer);
				}
				return;
			}
			#endif
			else if (complexTypesConverters.ContainsKey(objType))
			{
				complexTypesConverters[objType].ToBytes(obj, buffer);
				return;
			}
			// strings, arrays and other collections, run this same method on each item
			else if(obj is IEnumerable asEnumerable)
			{
				int count;

				if (obj is ICollection collection)
					count = collection.Count;
				else if (obj is string str)
					count = str.Length;
				else
				{
					Network.NetLog($"Find faster count resolve for {objType} if possible.");
					count = 0;
					foreach (object o in asEnumerable)
						count++;
				}
				// Add 255 to the buffer until our count is lower than 255 then add that last value, so there always will be a number between 0 & 254 as the last byte that is part of the count
				// When deserializing we will read and add the total until we reach a number not equal to 255 and add that last one to count before reading the following values based on that array

				// TODO : We can probably do better for very long arrays, what comes to mind is storing the count % in the first byte and count / in the other bytes,
				// for just two bytes of count we could take care of an array as large as 65025 elements
				// but thats not really necessary for now since I'm not passing big arrays and it will add an additional byte of bloat to small(<255) arrays

				while (count >= 255)
				{
					count -= 255;
					buffer.Add(255);
				}
				buffer.Add((byte)count);

				foreach (object o in asEnumerable)
				{
					ObjectToBytes(o, buffer);
				}
				return;
			}
			else if (objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>) )
			{
				Reflection.PropertyInfo key, value;
				if ((key = objType.GetProperty("Key")) != null && (value = objType.GetProperty("Key")) != null)
				{
					ObjectToBytes(key.GetValue(obj, null),   buffer);
					ObjectToBytes(value.GetValue(obj, null), buffer);
					return;
				}
				else
				{
					Network.NetLog($"{nameof(BinarySerializer)}: We seem to have a KVP type collection({objType}) but can't find the key property ?");
					return;
				}
			}
			throw new NotImplementedException ($"Serialization for type {objType} not implemented");
		}

		/// <summary>
		/// Use only if you can't find the type of your object
		/// </summary>
		/// <param name="objType"></param>
		/// <param name="bytes"></param>
		/// <param name="head"></param>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException"></exception>
		/// <exception cref="NotImplementedException"></exception>
		public static object BytesToObject_FObj(Type objType, byte[] bytes, ref int head)
		{
			if(objType == null)
				throw new InvalidOperationException($"{nameof(objType)} is null");


			if (objType.IsEnum)
			{
				if (enumValueTypeConversion.TryGetValue(objType, out Type valueType) == false)
				{
					valueType = Enum.GetUnderlyingType(objType);
					enumValueTypeConversion.Add(objType, valueType);
				}
				return Enum.ToObject(objType, BytesToObject_FObj(valueType, bytes, ref head));
			}
			else if (objType.IsValueType)
			{
				switch (Type.GetTypeCode(objType))
				{
					case TypeCode.Boolean:
						return BytesToObject<bool>(bytes, ref head);
					case TypeCode.Byte:
						return BytesToObject<byte>(bytes, ref head);
					case TypeCode.SByte:
						return BytesToObject<sbyte>(bytes, ref head);
					case TypeCode.Char:
						return BytesToObject<char>(bytes, ref head);
					case TypeCode.UInt16:
						return BytesToObject<ushort>(bytes, ref head);
					case TypeCode.UInt32:
						return BytesToObject<uint>(bytes, ref head);
					case TypeCode.UInt64:
						return BytesToObject<ulong>(bytes, ref head);
					case TypeCode.Int16:
						return BytesToObject<short>(bytes, ref head);
					case TypeCode.Int32:
						return BytesToObject<int>(bytes, ref head);
					case TypeCode.Int64:
						return BytesToObject<long>(bytes, ref head);
					case TypeCode.Decimal:
						return BytesToObject<decimal>(bytes, ref head);
					case TypeCode.Double:
						return BytesToObject<double>(bytes, ref head);
					case TypeCode.Single:
						return BytesToObject<float>(bytes, ref head);
					case TypeCode.DateTime:
						return BytesToObject<DateTime>(bytes, ref head);
					case TypeCode.String:
					case TypeCode.Object:
					case TypeCode.Empty:
					case TypeCode.DBNull:
					default:
						break;
				}
				#if USING_UNITY
				if(objType == typeof(Vector2))
					return BytesToObject<Vector2>(bytes, ref head);
				if(objType == typeof(Vector3))
					return BytesToObject<Vector3>(bytes, ref head);
				if(objType == typeof(Vector4))
					return BytesToObject<Vector4>(bytes, ref head);
				if(objType == typeof(Quaternion))
					return BytesToObject<Quaternion>(bytes, ref head);
				#endif
			}
			if (complexTypesConverters.ContainsKey(objType))
				return complexTypesConverters[objType].FromBytes(bytes, ref head);
			else if (typeof(IBinarySerializable<object>).IsAssignableFrom(objType))
			{
				IBinarySerializable<object> bseri = (IBinarySerializable<object>) Activator.CreateInstance(objType);
				return bseri.FromBytes(bytes, ref head);
			}
			#if USING_VGENERICS
			else if (typeof(VectorGenerics.IVectorGeneric).IsAssignableFrom(objType))
			{
				Type genericsType = objType.GenericTypeArguments[0];
				// Empty structs are invalid so we have to init it with a default value for the ctor
				object defaultInit = Activator.CreateInstance(genericsType);
				VectorGenerics.IVectorGeneric valueOut = (VectorGenerics.IVectorGeneric) Activator.CreateInstance(objType, defaultInit);
				for (int i = 0; i < valueOut.dimensions; i++)
				{
					valueOut[i] = BytesToObject_FObj(genericsType, bytes, ref head);
				}
				return valueOut;
			}
			#endif
			// strings, arrays and other collections
			else if(typeof(IEnumerable).IsAssignableFrom(objType))
			{
				int itemCount = 0;
				while (bytes[head] >= 255)
				{
					itemCount += 255;
					head++;
				}
				itemCount += bytes[head];
				head++;

				if (objType.IsArray)
				{
					Type itemType = objType.GetElementType();
					Array filledArray = Array.CreateInstance(itemType, itemCount);
					for (int i = 0; i < itemCount; i++)
					{
						filledArray.SetValue(BytesToObject_FObj(itemType, bytes, ref head), i);
					}
					return filledArray;
				}

				else if (objType == typeof(string))
				{
					System.Text.StringBuilder sBuilder = new System.Text.StringBuilder(itemCount);
					for (int i = 0; i < itemCount; i++)
					{
						sBuilder.Append(BytesToObject<char>(bytes, ref head));
					}
					return sBuilder.ToString();
				}

				else if (typeof(IList).IsAssignableFrom(objType))
				{
					Type itemType = objType.GetGenericArguments()[0];
					IList list = (IList) Activator.CreateInstance(objType);
					for (int i = 0; i < itemCount; i++)
					{
						list.Add(BytesToObject_FObj(itemType, bytes, ref head));
					}
					return list;
				}

				else if (typeof(IDictionary).IsAssignableFrom(objType))
				{
					IDictionary dic = (IDictionary) Activator.CreateInstance(objType);
					for (int i = 0; i < itemCount; i++)
					{
						dic.Add(BytesToObject_FObj(objType.GetGenericArguments()[0], bytes, ref head), BytesToObject_FObj(objType.GetGenericArguments()[1], bytes, ref head));
					}
					return dic;
				}

				throw new NotImplementedException($"{objType} item type fetching");
			}
			throw new NotImplementedException ($"Serialization for type {objType} not implemented");
		}






		public static T BytesToObject<T>(byte[] bytes, ref int head)
		{
			Type type = typeof(T);
			if (type.IsValueType)
			{
				new ByteConverter<T>(bytes, ref head, out T output);
				return output;
			}
			else
				return (T)BytesToObject_FObj(type, bytes, ref head);
		}
		public static void ObjectToBytes<T>(T value, ByteBuffer buffer)
		{
			if (typeof(T).IsValueType)
				new ENet.ByteConverter<T>(value, buffer);
			else
				ObjectToBytes_FObj(value, buffer);
		}






		public static void LazyObj2B(ByteBuffer buffer, params object[] o)
		{
			for (int i = 0; i < o.Length; i++)
			{
				ObjectToBytes_FObj(o[i], buffer);
			}
		}

		public static object[] LazyB2Obj(byte[] buffer, ref int head, params object[] o)
		{
			for (int i = 0; i < o.Length; i++)
			{
				o[i] = BytesToObject_FObj(o[i].GetType(), buffer, ref head);
			}
			return o;
		}

	}







	public class ByteBuffer
	{
		byte[] internalArray;
		public int internalIndex;
		public uint overflow;

		public int Capacity => internalArray.Length;
		public int Length => internalIndex;

		/// <param name="allowedOverflow">Additional array size added on each resize</param>
		public ByteBuffer(int initialSize, uint allowedOverflow)
		{
			internalArray = new byte[initialSize];
			overflow = allowedOverflow;
		}

		/// <summary>
		/// Prepare the array for a new add operation
		/// </summary>
		/// <param name="amountOfNewBytes"></param>
		public void Prepare(int amountOfNewBytes)
		{
			int goalLength = internalIndex + amountOfNewBytes;
			if (goalLength > internalArray.Length)
				Array.Resize(ref internalArray, goalLength);
		}



		public unsafe void Add<T>(T val) where T : unmanaged
		{
			var ptr = (byte*)&val;
			Prepare(sizeof(T));
			for( int i = 0; i < sizeof(T); i++ )
				Add_NoPrepare(ptr[i]);
		}



		public void Add(byte[] bytes)
		{
			unsafe
			{
				fixed (byte* pointerToArrayStart = &bytes[0])
				{
					PrepareAndReadPointer(pointerToArrayStart, bytes.Length);
				}
			}
		}


		public void Add(ByteBuffer buffer)
		{
			unsafe
			{
				fixed (byte* pointerToArrayStart = &buffer.internalArray[0])
				{
					PrepareAndReadPointer(pointerToArrayStart, buffer.Length);
				}
			}
		}

		/// <summary>
		/// This might fail if you didn't prepare the class before adding
		/// </summary>
		/// <param name="newByte"></param>
		public void Add_NoPrepare(byte newByte)
		{
			internalArray[internalIndex++] = newByte;
		}

		public unsafe void PrepareAndReadPointer(byte* pointerToNewData, int dataLength)
		{
			int goalLength = internalIndex + dataLength;
			if (goalLength > internalArray.Length)
				Array.Resize(ref internalArray, goalLength + (int)overflow);

			fixed (byte* pointerToFreeArraySpace = &internalArray[internalIndex])
			{
				switch (dataLength)
				{
					case 0:
						return;
					case sizeof(byte):
						*pointerToFreeArraySpace = *pointerToNewData;
						break;
					case sizeof(short):
						*((short*)pointerToFreeArraySpace) = *(short*)pointerToNewData;
						break;
					case Three.sizeInBytes:
						*((Three*)pointerToFreeArraySpace) = *(Three*)pointerToNewData;
						break;
					case sizeof(int):
						*((int*)pointerToFreeArraySpace) = *(int*)pointerToNewData;
						break;
					case sizeof(long):
						*((long*)pointerToFreeArraySpace) = *(long*)pointerToNewData;
						break;
					case sizeof(decimal):
						*((decimal*)pointerToFreeArraySpace) = *(decimal*)pointerToNewData;
						break;
					default:
						Buffer.MemoryCopy(pointerToNewData, pointerToFreeArraySpace, internalArray.Length-internalIndex, dataLength);
						break;
				}
				internalIndex += dataLength;
			}
		}

		/// <summary>
		/// Resets the head of the array, doesn't resize the array
		/// </summary>
		public void Clear()
		{
			internalIndex = 0;
		}


		/// <summary>
		/// Resize the internal array to remove any unnecessary bytes
		/// </summary>
		/// <returns></returns>
		public void Trim()
		{
			Array.Resize(ref internalArray, internalIndex);
		}


		/// <summary>
		/// Resize the internal array to remove any unnecessary bytes and returns it.
		/// </summary>
		/// <returns></returns>
		public byte[] TrimAndOutput()
		{
			Trim();
			return internalArray;
		}


		/// <summary>
		/// outputs the internal array, it might be larger than the amount of bytes you added to it,
		/// the rest of the data is only there to reduce the amount of resize operation when adding to it
		/// </summary>
		/// <returns></returns>
		public byte[] Output()
		{
			return internalArray;
		}

		/// <summary>
		/// outputs the internal array and gives the length of valid data,
		/// the rest of the data is only there to reduce the amount of resize operation when adding to it
		/// </summary>
		/// <returns></returns>
		public byte[] Output(out int length)
		{
			length = internalIndex;
			return internalArray;
		}
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Three
	{
		byte a, b, c;
		public const int sizeInBytes = 3;
	}





	public static class ByteConverter
	{
		public static void Test()
		{
			ByteBuffer   buffer = new ByteBuffer(4, 10);
			int          head   = 0;
			string[] objOut =
			{
				Test(true,                                  buffer, ref head),
				Test((byte)124,                             buffer, ref head),
				Test((sbyte)12,                             buffer, ref head),
				Test('g',                                   buffer, ref head),
				Test((short)-102,                           buffer, ref head),
				Test((ushort)253,                           buffer, ref head),
				Test(10.01f,                                buffer, ref head),
				Test((int)-102445,                          buffer, ref head),
				Test((uint)402453,                          buffer, ref head),
				Test((double)21210.25d,                     buffer, ref head),
				Test((long)-95224577741,                    buffer, ref head),
				Test((ulong)1595244570141,                  buffer, ref head),
				Test((decimal)5554778112.25654,             buffer, ref head),
				#if USING_UNITY
				Test(new Vector2(-10f, 50f),                buffer, ref head),
				Test(new Vector3(552f, 127f, 33f),          buffer, ref head),
				Test(new Vector4(-81f, 225f, 40f, 50f),     buffer, ref head),
				Test(new Quaternion(0.5f, 0.1f, 0.0f, -1f), buffer, ref head),
				#endif
			};
			Network.NetLog(string.Concat(objOut, "\n"));
		}

		public static string Test<T>(T value, ByteBuffer buffer, ref int head)
		{
			int previousHead = head;

			// ToBytes
			new ByteConverter<T>(value, buffer);

			// ToStruct
			new ByteConverter<T>(buffer.Output(), ref head, out T outValue);
			return $"{value} => {outValue} ({(head -previousHead)} bytes {typeof(T)})";
		}
	}


	/// <summary>
	/// Be carefull when using this type, it might corrupt your program if not used correctly !
	/// Helper to convert structs to their raw byte representation,
	/// approximatively 6 times faster than BitConverter,
	/// doesn't generate any garbage, works with any custom structs containing value types.
	/// Having reference types inside your structs will most probably corrupt your program.
	/// <br/>
	/// If you want to avoid wasted bytes in your custom structs, you'll have to specify a proper structlayout
	/// attribute with a pack of 1, you should also specify explicitly the value types of your enums since they default to ints.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct ByteConverter<T>
	{
		// Do not modify those field's order, the order in which they are placed is essential

		/* HIGH LEVEL:
		 * Once the constructor is created, we assign the generic given in the constructor to
		 * our generic field, we then go to the memory to read each one of its bytes.
		 * When assigning bytes we just create a default value for the generic and fill it with
		 * the bytes from the array
		 */
		/* LOW LEVEL:
		 * This structure is tightly packed (LayoutKind.Sequential & Pack = 1), each one of its fields
		 * is closely followed in memory by the next one. Since we can't get a pointer to a generic variable
		 * we just take the pointer to the variable before it (memStart) and offset it by one since its a byte
		 * to then be on the start of the generic variable (value), since the generic is also a struct, all
		 * of its variable are layed out next to each other so we just have to then retrieve the pointer to
		 * our last field (memEnd) to find out where the generic stops. We then read the memory and assign
		 * the bytes contained between those two pointers to the provided array.
		 *
		 * When retrieving a value from bytes, we assign a default value for the generic, set the
		 * memory instead of reading from it and then we output the value.
		 */
		byte memStart;
		T value;
		byte memEnd;

		public ByteConverter(byte[] bytes, ref int head, out T outValue)
		{
			memStart = 121;
			memEnd   = 212;
			value    = default(T);
			unsafe
			{
				fixed (byte* pStart = &memStart, pEnd = &memEnd, arrayHeadPointer = &bytes[head])
				{
					byte* valuePointer = pStart + 1;
					int length = (int) (pEnd - valuePointer);
					switch (length)
					{
						case 0:
							break;
						case sizeof(byte):
							*valuePointer = *arrayHeadPointer;
							break;
						case sizeof(short):
							*((short*)valuePointer) = *(short*)arrayHeadPointer;
							break;
						case Three.sizeInBytes:
							*((Three*)valuePointer) = *(Three*)arrayHeadPointer;
							break;
						case sizeof(int):
							*((int*)valuePointer) = *(int*)arrayHeadPointer;
							break;
						case sizeof(long):
							*((long*)valuePointer) = *(long*)arrayHeadPointer;
							break;
						case sizeof(decimal):
							*((decimal*)valuePointer) = *(decimal*)arrayHeadPointer;
							break;
						default:
							Buffer.MemoryCopy(arrayHeadPointer, valuePointer, length, length);
							break;
					}
					head += length;
				}
			}
			outValue = value;
		}

		public ByteConverter(T inValue, ByteBuffer buffer)
		{
			memStart = 121;
			memEnd   = 212;
			value    = inValue;
			unsafe
			{
				fixed (byte* pStart = &memStart, pEnd = &memEnd)
				{
					byte* valuePointer = pStart + 1;
					int length = (int) (pEnd - valuePointer);
					buffer.PrepareAndReadPointer(valuePointer, length);
				}
			}
		}
	}

	public class EndPointConverter
	{
		public static void ToBytes(object baseObject, ByteBuffer input)
		{
			IPEndPoint obj = (IPEndPoint)baseObject;
			BinarySerializer.ObjectToBytes((object)obj.Address, input);
			BinarySerializer.ObjectToBytes(obj.Port, input);
		}

		public static object FromBytes(byte[] input, ref int head)
		{
			return new IPEndPoint(IPAddress.Parse(BinarySerializer.BytesToObject<string>(input, ref head)), BinarySerializer.BytesToObject<int>(input, ref head));
		}
	}

	public interface IBinarySerializable<out T>
	{
		void ToBytes(ByteBuffer buffer);

		T FromBytes(byte[] buffer, ref int head);
	}

}