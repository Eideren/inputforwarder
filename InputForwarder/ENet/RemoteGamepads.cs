namespace ENet
{
	using System.Collections.Generic;
	using InputForwarder.Hardware;
	using LiteNetLib;
	using SharpDX.XInput;



	public static class RemoteGamepads
	{
		static readonly Dictionary<NetPeer, GamepadSource[]> _sources = new Dictionary<NetPeer, GamepadSource[]>();
		static RemoteGamepads()
		{
			Network.OnPeerDisconnect += (p, i) => DisconnectPeer(p);
			
			void DisconnectPeer( NetPeer peer )
			{
				lock( _sources )
				{
					if( _sources.TryGetValue( peer, out var sources ) )
						_sources.Remove( peer );
				}
			}
		}



		public static bool NewStateFor( NetPeer peer, Gamepad state, int index, out GamepadSource outSource )
		{
			lock( _sources )
			{
				if( _sources.TryGetValue( peer, out var sources ) == false )
					_sources.Add( peer, sources = new GamepadSource[4] );

				ref var source = ref sources[ index ];
				var wasNew = source == null;
				source ??= new GamepadSource( peer, index );
				source.State = state;
				outSource = source;
				return wasNew;
			}
		}



		public static List<GamepadSource> SampleRegisteredSources()
		{
			var sources = new List<GamepadSource>();
			lock( _sources )
			{
				foreach( var peer in _sources )
				{
					for( int i = 0; i < peer.Value.Length; i++ )
					{
						var source = peer.Value[i];
						if( source == null )
							continue;
						sources.Add( source );
					}
				}

			}

			return sources;
		}
	}
}